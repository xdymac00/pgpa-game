#pragma once
#include <glad\glad.h>
#include <GLFW\glfw3.h>
#include <glm/glm.hpp>
#include  <glm/gtc\type_ptr.hpp>


#include "Shader.h"

class ShaderProgram
{
public:
	ShaderProgram();
	~ShaderProgram();
	bool addShader(char* shaderPath, GLenum shaderType);
	bool linkProgram();
	void deleteShaders();
	void useProgram();

	//seting uniform variables
	void setUniformVariable(const std::string &name, bool value) const;
	void setUniformVariable(const std::string &name, GLint value) const;
	void setUniformVariable(const std::string &name, GLfloat value) const;
	void setUniformVariable(const std::string &name, glm::vec2  value) const;
	void setUniformVariable(const std::string &name, glm::vec4  value) const;
	void setUniformVariable(const std::string &name, glm::vec3  value) const;
	void setUniformVariable(const std::string &name, glm::mat4  value) const;
	void setUniformVariable(const std::string &name, std::vector<glm::mat4>  &value) const;
	void setUniformVariable(const std::string &name, std::vector<glm::vec3>  &value) const;
	
	GLuint getShaderProgram() const;

private:
	std::vector<Shader*> _shaders;
	GLuint _shaderProgram = 0;
};

