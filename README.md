Potřebné knihovny:
    GLAD - https://glad.dav1d.de/
    GLFW - https://www.glfw.org/
    GLM - https://glm.g-truc.net/0.9.9/index.html
    Assimp  - http://www.assimp.org/
    STB image - https://github.com/nothings/stb/blob/master/stb_image.h
    
Složky Models a Shaders spolu s jejich obsahy musí být při spuštění ve stejné složce jako hra.