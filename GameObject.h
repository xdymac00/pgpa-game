#pragma once

#include <glm/glm.hpp>
#include <glm/gtc\matrix_transform.hpp>
#include <vector>
#include <memory>
#include <algorithm>
#include <functional>

#include "Model.h"
#include "ShaderProgram.h"
#include "Light.h"

#include "Particles.h"

class Model;

class GameObject :std::enable_shared_from_this<GameObject>
{
public:
	GameObject(std::shared_ptr<Model> model);
	static std::shared_ptr<GameObject> createObject(std::shared_ptr<Model> model);
	//void init(std::shared_ptr<GameObject> thi);
	~GameObject();

	virtual void draw(const ShaderProgram* shaderProgram) const;
	glm::mat4 getObjectMatrix();
#ifdef DRAW_BOUNDING_BOXES
	void drawBoundingBox(const ShaderProgram* shaderProgram);
#endif //DRAW_BOUNDING_BOXES

	bool colide(GameObject* gameObject);
#ifdef BOUNDING_VOLUMES
	bool colideWith(GameObject* gameObject);
#endif //BOUNDING_VOLUMES

	virtual void translate(const glm::vec3 value);
	virtual void scale(const glm::vec3 value);
	virtual void rotate(const glm::vec3 value, const float angle);		//ve stupnich

	virtual void translateAnim(const glm::vec3 value);
	virtual void scaleAnim(const glm::vec3 value);
	virtual void rotateAnim(const glm::vec3 value, const float angle);		//ve stupnich
	virtual void resetAnim();
	virtual void setAnimation(std::function<void(float)> value);
	virtual void animate(float time);

	glm::mat4 getIdentityMatrix() const;
	void setModelMatrix(glm::mat4 value);
	glm::mat4 getModelMatrix() const;

	//static int nextID;
	//int id;
	virtual void onColision(GameObject* object);
	virtual bool coliding(); //return true if object can colide with other
	bool isPointAbove(glm::vec3 point) const;

	void setPeakHeight(float height);
	float getPeakheight();

	bool generateTexture = false;

	//void saveToVector(std::vector<std::shared_ptr<GameObject>>* vector, std::shared_ptr<GameObject> thi);

	bool deleteThis = false;	//if true this object is ready for delete

private:	
	std::shared_ptr<Model> _model;
	//std::vector<std::vector<std::shared_ptr<GameObject>>*> _savedIn;
	
	glm::mat4 _modelMatrix;
	glm::mat4 _animationMatrix;
	//bool _objectChange = false;
	std::function<void(float)> _animation = [](float time) { return; };
	virtual float getGSstate() const;

	BoundingBox _boundingBox;
	void calculateAxisAlignedBoundingBox();

	glm::mat4 _identityMatrix = glm::mat4(0);

	float peakHeight = 0;

#ifdef BOUNDING_VOLUMES
	BoundingVolume _boundingVolume;
	void transformBoundingVolume();
#endif // BOUNDING_VOLUMES
};

//////// GameObjectPlayer ////////
class GameObjectPlayer : public GameObject
{
public:
	GameObjectPlayer(std::shared_ptr<Model> model);
	static std::shared_ptr<GameObject> createObject(std::shared_ptr<Model> model);
	glm::vec3 getPosition() const;
	glm::vec3 getFront() const;
	int getScore();

	void pickPoint();
	void animate(float time);
	void draw( const ShaderProgram * shaderProgram) const;
	void jump() { _jump = true; };
	void move() { _move = true; };

	std::shared_ptr<Particles> _pointParticles;

private:
	glm::vec3 _position = glm::vec3(0.0, 0.0, 0.0);
	glm::vec3 _front = glm::vec3(0.0, 0.0, 1.0);
	
	bool _jump = false;
	bool _move = false;

	int _points = 0;
};

//////// GameObjectCoin ////////
class GameObjectCoin : public GameObject
{
public:
	GameObjectCoin(std::shared_ptr<Model> model);
	static std::shared_ptr<GameObject> createObject(std::shared_ptr<Model> model);
	~GameObjectCoin();

	void onPickUp();
	virtual void onColision(GameObject* object);
	void animate(float time);
	bool coliding();
	void draw( const ShaderProgram * shaderProgram) const;
	std::shared_ptr<PointLight> _light;
private:
	float lastTime = 0;
	bool _pickedUp = false;
	float getGSstate() const;
	float _gsState = 0.0;
};
