#include "Mesh.h"



Mesh::Mesh()
{
}


Mesh::~Mesh()
{
	if (_material != nullptr) delete _material;
	glDeleteBuffers(1, &_VAO);
	glDeleteBuffers(1, &_VBO);
	glDeleteBuffers(1, &_EBO);
}

void Mesh::init()
{
	glGenVertexArrays(1, &_VAO);
	glGenBuffers(1, &_VBO);
	glGenBuffers(1, &_EBO);
//	glGenBuffers(1, &_instances);

	//glBindVertexArray(_VAO);
	//glNamedBufferData(_VBO, _vertices.size() * sizeof(Vertex), _vertices.data(), GL_STATIC_DRAW);
	//glNamedBufferData(_EBO, _indices.size() * sizeof(unsigned int), _indices.data(), GL_STATIC_DRAW);

	glBindVertexArray(_VAO);
	glBindBuffer(GL_ARRAY_BUFFER, _VBO);
	glBufferData(GL_ARRAY_BUFFER, _vertices.size() * sizeof(Vertex), &_vertices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, _indices.size() * sizeof(unsigned int), &_indices[0], GL_STATIC_DRAW);


	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Position));
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Normal));
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, TexCoords));
	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Tangent));
/*
	glEnableVertexAttribArray(4);
	glEnableVertexAttribArray(5);
	glEnableVertexAttribArray(6);
	glEnableVertexAttribArray(7);

	glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)0);	
	glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(sizeof(glm::vec4)));	
	glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(2 * sizeof(glm::vec4)));	
	glVertexAttribPointer(7, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(3 * sizeof(glm::vec4)));

	glVertexAttribDivisor(4, 1);
	glVertexAttribDivisor(5, 1);
	glVertexAttribDivisor(6, 1);
	glVertexAttribDivisor(7, 1);
	*/
	glBindVertexArray(0);

#ifdef BOUNDING_VOLUMES
	_boundingVolume.calculateVertices();
#endif //BOUNDING_VOLUMES
#ifdef DRAW_BOUNDING_BOXES
	_boundingBox.initForDraw();
#endif // DRAW_BOUNDING_BOXES

}

void Mesh::draw(const ShaderProgram* shaderProgram) const
{
	_material->useMaterial(shaderProgram);

	glBindVertexArray(_VAO);

	glDrawElements(GL_TRIANGLES, _indices.size(), GL_UNSIGNED_INT, 0);

	glBindVertexArray(0);
}

void Mesh::instanceDrawInit(const ShaderProgram * shaderProgram) const
{
	_material->useMaterial(shaderProgram);
	shaderProgram->setUniformVariable("texMultiplier", _texCoordMultiplier);

	glBindVertexArray(_VAO);
}

void Mesh::instanceDrawEnd() const
{
	glBindVertexArray(0);
}

#ifdef DRAW_BOUNDING_BOXES
void Mesh::drawBoundingBox(const ShaderProgram * shaderProgram)
{
	_boundingBox.draw(shaderProgram);
}
#endif //DRAW_BOUNDINX_BOXES

void Mesh::pushVertex(const Vertex vertex)
{
	_vertices.push_back(vertex);
	_boundingBox.expand(vertex);
#ifdef BOUNDING_VOLUMES
	_boundingVolume.expand(vertex.Position);
#endif // BOUNDING_VOLUMES
}

void Mesh::pushIndex(const unsigned int index)
{
	_indices.push_back(index);
}
void Mesh::setMaterial(Material * value)
{
	_material = value;
}

const BoundingBox * Mesh::getBoundingBox()
{
	return &_boundingBox;
}

void Mesh::setShinines(int value)
{
	_material->setShinines(value);
}
int Mesh::size()
{
	return _indices.size();
}
#ifdef BOUNDING_VOLUMES
BoundingVolume * Mesh::getBoundingVolume()
{
	return &_boundingVolume;
}
#endif //BOUNDING_VOLUMES
