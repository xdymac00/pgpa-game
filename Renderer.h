#pragma once
#include <glad\glad.h>
#include <GLFW\glfw3.h>
#include <glm/glm.hpp>
#include  <glm/gtc\type_ptr.hpp>
#include <glm/gtc\matrix_transform.hpp>
//#include <gtx\vector_angle.hpp>
//#include <gtx\rotate_vector.hpp>
#include <xtgmath.h>
#include <iostream>
#include <memory>
#include <cstdlib>
#include <ctime>
#include <random>

#include "ShaderProgram.h"
#include "Camera.h"
#include "Model.h"
#include "GameObject.h"
#include "Light.h"
#include "Skybox.h"
#include "pointLights.h"
#include "Particles.h"

#include "DebugDefines.h"

#define WINDOW_NAME "window"

#define PLAYER 0

#define TOWERCOUNT 200

class Renderer
{
public:
	Renderer(int w, int h);
	~Renderer();

	bool initialize();
	void mainLoop();
	
private:
	std::vector<float> measurements;

	void processPlayer();
	void debugCameraControl();

	void initGbuffer();
	unsigned int _gBuffer, _gPosition, _gNormal, _gColorSpec, _gAlbedoSpec, _rboDepth;
	void initHdrBuffer();
	unsigned int _hdrFrameBuffer, _hdrCollorBuffer, _hdrDepthBuffer;
	unsigned int _bloomFrameBuffer[2], _bloomCollorBuffer[2];
	void initSSAO();
	unsigned int _ssaoNoiseTexture, _ssaoFrameBuffer, _ssaoColorBuffer, _ssaoBlurFramebuffer, _ssaoBlurColor;
	std::vector<glm::vec3> _ssaoNoise;
	std::vector<glm::vec3> _ssaoKernel;

	//okno
	GLFWwindow* _window;
	int _width;
	int _height;
	
	//camera
	Camera _camera;
	float _cameraYOffset = -0.5;

	std::unique_ptr<ShaderProgram> _shaderProgramGeometryPass;
	std::unique_ptr<ShaderProgram> _shaderProgramLightingPass;
	std::unique_ptr<ShaderProgram> _shaderProgramLava;
	std::unique_ptr<ShaderProgram> _shaderProgramLightingPassDirectional;
	std::unique_ptr<ShaderProgram> _shaderProgramPostProcess;
	std::unique_ptr<ShaderProgram> _shaderProgramBloom;
	std::unique_ptr<ShaderProgram> _shaderProgramSSAO;
	std::unique_ptr<ShaderProgram> _shaderProgramSSAOblur;
	std::unique_ptr<ShaderProgram> _shaderProgramParticleEffects;
	std::unique_ptr<ShaderProgram> _shaderProgramScore;
	/*std::unique_ptr<ShaderProgram> _shaderProgramSkybox;
	std::unique_ptr<ShaderProgram> _shaderProgramNew;*/

	void createScene();
	void clearGameObjects();
	std::vector<std::shared_ptr<Model>> _models;
	std::vector<std::shared_ptr<GameObject>> _gameObjects;
	std::vector<std::shared_ptr<Particles>> _particles;
	std::shared_ptr<GameObject> _debug;
	std::vector<std::shared_ptr<PointLight>> _pointLights;
	std::vector<std::shared_ptr<DirectionLight>> _directionLight;
	std::shared_ptr<GameObject> _skyboxModel;
	std::shared_ptr<Skybox> _skybox;
	std::shared_ptr<GameObject> _floor;
	std::shared_ptr<GameObject> _crater;
	std::unique_ptr<PointLights> _pointLightsO;

	std::vector<std::shared_ptr<GameObject>> _score;

	void framebufferSizeChangeCallback(GLFWwindow* window, int width, int height);

	float _exposure = 1.0;
	bool _toneMappingEnable = true;
	float _bloomEnable = true;
	bool _enableDirLight = true;
	bool _enablePointLight = true;
	bool _enableFog = true;
	bool _enableParalax = true;
	float _paralaxStr = 0.02;
	bool _enableSSAO = true;
	bool _enableParticles = true;
	float _ssaoRadius = 0.5;
	int _test = 0;

	//mouse input
	bool _firstMouse = true;
	float lastX = 0.0f;
	float lastY = 0.0f;
	float tmpi = 0;
	void mouseCallbackM(GLFWwindow* window, double xpos, double ypos);

	//frame time logic
	double _lastFrameTime = 0.0f;
	float _deltaFrameTime = 0.0f;
	void frametimeLogic();

	//player movement

	/*sily na krychly*/
	glm::vec3 movementVector = glm::vec3(0.0, 0.0, 0.0);
	glm::vec2 velocity = glm::vec2(0.0, 0.0);
	
	/*skoky*/
	int spacePressedCount = 0;
	float spacePressed = 0.0;

	/*fyzikalni koeficienty*/
	float gravity = 30.0; 
	float frictionCoefitient = 20.0;
	float accelerationCoefficient = 70.0;
	float jumpForce = 30.0;
	float maxSpeed = 20.0;
	float rotationSpeed = 100;


	int onlyBackwardsAfterCollision = 0;
	int onlyForwardsAfterCollision = 0;

	void generateGameBoard();
	float generateRandomInt(int min, int max);

	void click(int keyEnum, std::function<void()> event, std::function<void()> elseEvent = nullptr);
	void press(int keyEnum, std::function<void()> event, std::function<void()> elseEvent = nullptr);
	bool turnProceduralOff = false;
	bool _displayScore = true;
#ifdef DEBUG
	std::unique_ptr<ShaderProgram> _shaderProgramDebugElements;
#endif // DEBUG
#ifdef DRAW_BOUNDING_BOXES
	bool turnBBoff = true;
#endif // DRAW_BOUNDING_BOXES

};

