﻿#include "Model.h"



Model::Model(const char* path)
{
	
	std::size_t found = std::string(path).find_last_of("/\\");
	std::string  dpath = std::string(path).substr(0, found);

	Assimp::Importer importer;
	const aiScene *scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_CalcTangentSpace);

	if (!scene) {
		throw std::runtime_error("assimp scene load error");
	}

	processNode(scene->mRootNode, scene, dpath); 
	//glGenBuffers(1, &_instanceBuffer);
	//std::string label = "instanceBuffer";
	//glObjectLabel(GL_BUFFER, _instanceBuffer, label.length(), label.c_str());
#ifdef BOUNDING_VOLUMES
	_boundingVolume.calculateVertices();
#endif //BOUNDING_VOLUMES
#ifdef DRAW_BOUNDING_BOXES
	_boundingBox.initForDraw();
#endif //DRAW_BOUNDING_BOXES
}


Model::~Model()
{
	for (auto &mesh : _meshes) {
		delete mesh;
	}
	glDeleteBuffers(1, &_instanceBuffer);
}

void Model::processNode(aiNode * node, const aiScene * scene, std::string path)
{
	for (unsigned int i = 0; i < node->mNumMeshes; i++) {
		aiMesh *mesh = scene->mMeshes[node->mMeshes[i]];
		Mesh* procesed = processMesh(mesh, scene, path);
		procesed->init();
		_boundingBox.expand(procesed->getBoundingBox());
#ifdef BOUNDING_VOLUMES
		_boundingVolume.expand(procesed->getBoundingVolume());
#endif //BOUNDING_VOLUMES
		_meshes.push_back(procesed);
		//_meshes.push_back(processMesh(mesh, scene));
		//_meshes.back().init();
	}

	for (unsigned int i = 0; i < node->mNumChildren; i++) {
		processNode(node->mChildren[i], scene, path);
	}


	for (auto &mesh : _meshes) {
		glBindVertexArray(mesh->_VAO);
		glBindBuffer(GL_ARRAY_BUFFER, _instanceBuffer);

		glEnableVertexAttribArray(4);
		glEnableVertexAttribArray(5);
		glEnableVertexAttribArray(6);
		glEnableVertexAttribArray(7);

		glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)0);
		glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(sizeof(glm::vec4)));
		glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(2 * sizeof(glm::vec4)));
		glVertexAttribPointer(7, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(3 * sizeof(glm::vec4)));

		glVertexAttribDivisor(4, 1);
		glVertexAttribDivisor(5, 1);
		glVertexAttribDivisor(6, 1);
		glVertexAttribDivisor(7, 1);

		glBindVertexArray(0);
	}
}

Mesh* Model::processMesh(aiMesh * meshAi, const aiScene * scene, std::string path)
{
	glGenBuffers(1, &_instanceBuffer);

	Mesh* mesh = new Mesh();

	//vertices
	for (unsigned int i = 0; i < meshAi->mNumVertices; i++) {
		Vertex vertex = { {0.0, 0.0, 0.0},{ 0.0, 0.0, 0.0 },{ 0.0, 0.0 },{ 0.0, 0.0, 0.0 },{ 0.0, 0.0, 0.0, 0.0 } };
		vertex.Position =	aiVecToGlmVec(meshAi->mVertices[i]);
		vertex.Normal =		aiVecToGlmVec(meshAi->mNormals[i]);
		if (meshAi->mTangents != nullptr)
			vertex.Tangent = aiVecToGlmVec(meshAi->mTangents[i]);
		if (meshAi->mTextureCoords[0])
			vertex.TexCoords = aiVecToGlmVec(meshAi->mTextureCoords[0][i]);
		else
			vertex.TexCoords = glm::vec2(0.0f, 0.0f);
		
		mesh->pushVertex(vertex);
		//std::cout	<< vertex.Position.x << ", "	<< vertex.Position.y	<< ", " << vertex.Position.z << ", " 
					//<< vertex.Normal.x << ", "		<< vertex.Normal.y		<< ", " << vertex.Normal.z << ", "
					//<< vertex.Tangent.x << ", "		<< vertex.Tangent.y		<< ", " << vertex.Tangent.z << ", "
					//<< vertex.TexCoords.x << ", "	<< vertex.TexCoords.y	<< std::endl
		//;
	}

	//indices
	for (unsigned int i = 0; i < meshAi->mNumFaces; i++) {
		aiFace face = meshAi->mFaces[i];
		for (unsigned int j = 0; j < face.mNumIndices; j++) {
			mesh->pushIndex(face.mIndices[j]);
			//std::cout << face.mIndices[j] << ", ";
		}
	}


	//materials
	Material *material = new Material(scene->mMaterials[meshAi->mMaterialIndex], path);
	mesh->setMaterial(material);

	return mesh;
}

glm::vec3 Model::aiVecToGlmVec(aiVector3D vectorIn)
{
	return glm::vec3(vectorIn.x, vectorIn.y, vectorIn.z);
}

glm::vec2 Model::aiVecToGlmVec(aiVector2D vectorIn)
{
	return glm::vec2(vectorIn.x, vectorIn.y);
}

void Model::clearInstances()
{
	_instances.erase(std::remove_if(_instances.begin(), _instances.end(), [](std::weak_ptr<GameObject> const &object) { return (object.expired() || !object.lock()); }), _instances.end());
}

void Model::draw(const ShaderProgram* shaderProgram) const
{
	for (const auto &mesh : _meshes) {
		mesh->draw(shaderProgram);
	}
}

void Model::drawInstanced(const ShaderProgram * shaderProgram)
{
	if (!_drawInstanced) { return; }

	std::vector<glm::mat4> matrices;
	int i = 0;
	clearInstances();	//all instances are valid
	for (auto &instance : _instances) {
		std::shared_ptr<GameObject> shInstance = instance.lock();
		matrices.push_back(shInstance->getObjectMatrix());	
		i++;
	}
	glBindBuffer(GL_ARRAY_BUFFER, _instanceBuffer);
	glBufferData(GL_ARRAY_BUFFER, matrices.size() * sizeof(glm::mat4), matrices.data(), GL_DYNAMIC_DRAW);
	shaderProgram->setUniformVariable("inverseNormals", _inverseNormals);
	for (const auto &mesh : _meshes) {
		mesh->instanceDrawInit(shaderProgram);
		glDrawElementsInstanced(GL_TRIANGLES, mesh->size(), GL_UNSIGNED_INT, 0, /*_instances.size()*/ matrices.size());
		mesh->instanceDrawEnd();
	}
}

std::shared_ptr<GameObject> Model::createInstance(types type)
{
	std::shared_ptr<GameObject> ret;
	switch (type)
	{
	case Model::DEFAULT:		
		ret = GameObject::createObject(shared_from_this()); //std::make_shared<GameObject>(shared_from_this());
		break;
	case Model::PLAYE:
		ret = GameObjectPlayer::createObject(shared_from_this());
		break;
	case Model::COIN:
		ret = GameObjectCoin::createObject(shared_from_this());
		break;
	default:
		break;
	}
	_instances.push_back(ret);
	return ret;
}
/*
void Model::addInstance(GameObject* instance)
{
	_instances.push_back(instance);
}*/
/*
void Model::updateInstance(glm::mat4* instance, glm::mat4 objectMatrix)
{
	*instance = objectMatrix;
}

void Model::removeInstance(glm::mat4 *instance)
{
	//_instances.erase(_instances.begin() + instance);
	for (std::vector<glm::mat4>::iterator i = _instances.begin(); i != _instances.end(); ++i) {
		if (&(*i) == instance) {
			_instances.erase(i);
		}
	}
}
*/
const BoundingBox * Model::getBoundingBox() const
{
	return &_boundingBox;
}

void Model::setShinines(int value)
{
	for (auto &mesh : _meshes) {
		mesh->setShinines(value);
	}
}

void Model::setTexCoordMultiplier(float value)
{
	for (auto &mest : _meshes) {
		mest->_texCoordMultiplier = value;
	}
}

#ifdef BOUNDING_VOLUMES
const BoundingVolume * Model::getBoundingVolume() const
{
	return &_boundingVolume;
}
#endif //BOUNDING_VOLUMES

#ifdef DRAW_BOUNDING_BOXES
void Model::drawBoundingBox(const ShaderProgram * shaderProgram)
{
	_boundingBox.draw(shaderProgram);	//draw model BoundingBox
	for (const auto &mesh : _meshes) {	//draw meshes BoundingBox
		mesh->drawBoundingBox(shaderProgram);
	}
}
#endif //DRAW_BOUNDING_BOXES
