#version 450

//input
in vec2 texCoordinates;

// output
out vec4 fragColor;

uniform sampler2D scene;
uniform sampler2D bloom;
uniform sampler2D gPosition;

uniform vec3 cameraPos;
uniform float exposure;
uniform bool tmEnable;
uniform bool fogEnable;

vec3 fog(vec3 color);

void main() 
{
	const float gamma = 2.2;
    vec3 color = texture(scene, texCoordinates).rgb;
    color += texture(bloom, texCoordinates).rgb;
  
    // Exposure tone mapping
    if (tmEnable)
    	color = vec3(1.0) - exp(-color * exposure);

    if (fogEnable)
		color = fog(color);

    // Gamma correction 
    color = pow(color, vec3(1.0 / gamma));

	fragColor = vec4(color, 1.0); // vec4(texture(scene, texCoordinates).xyz, 1.0); 
	//fragColor = vec4(1.0, 0.0, 0.0, 1.0); 
	//fragColor = vec4(texture(gPosition, texCoordinates).rgb, 1.0);
}

vec3 fog(vec3 color) {
	float fog_maxdist = 300.0;
	float fog_mindist = 150.0;
	vec4  fog_colour = vec4(0.9179, 0.042, 0.0507, 1.0); //vec4(0.4, 0.4, 0.4, 1.0);
	vec3 fragPos = texture(gPosition, texCoordinates).rgb;
	float height = fragPos.y + 1;
	float dist = length(cameraPos - fragPos);

	float yFactor =1 - 1 / (1+height*height);
	float fog_factor = (fog_maxdist - dist) / (fog_maxdist - fog_mindist);
	fog_factor = clamp(1-((1-fog_factor) * (1-yFactor)), 0.7, 1.0);
	//yFactor = clamp( yFactor , 0.0, 1.0);

	return vec3(mix(fog_colour, vec4(color, 1.0), fog_factor ));
}
