#version 400

struct Material {
	sampler2D diffuse;
	float shininess;
};

uniform Material material;

uniform int number;
uniform vec3 viewPos;	


in	vec3 normal_v;
//	vec3 fragPos_v;
in	vec2 texCoordinates_v;

out vec4 fragColor;

void main() 
{
	fragColor = vec4(vec3(texture(material.diffuse, vec2(texCoordinates_v.x + 0.1 * number, texCoordinates_v.y ))) , 1.0);

}
