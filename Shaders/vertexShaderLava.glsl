#version 450

layout(location = 0) in vec3 in_Position;
layout(location = 1) in vec3 in_normal;
layout(location = 2) in vec2 in_tex_coordinates;

uniform mat4 modelMatrix;
uniform mat4 projectionMatrix;
uniform mat4 cameraMatrix;

out vec3 normal;
out vec3 fragPos;
out vec2 texCoordinates;

void main() 
{
	fragPos = vec3(modelMatrix * vec4(in_Position, 1.0));
	normal = mat3(transpose(inverse(modelMatrix))) * in_normal;

	texCoordinates = in_tex_coordinates;
	gl_Position = projectionMatrix * cameraMatrix * vec4(fragPos, 1.0);
}