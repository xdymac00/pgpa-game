#version 450

// Inputs
layout(location = 0) in vec3 in_Position;
layout(location = 1) in vec3 in_normal;
layout(location = 2) in vec2 in_tex_coordinates;
layout(location = 3) in vec3 in_tangent;
layout(location = 4) in mat4 modelMatrix;

// Uniforms
//uniform mat4 projectionMatrix;
//uniform mat4 cameraMatrix;
uniform mat4 VC;
uniform vec3 viewPos;
uniform bool inverseNormals;

// Outputs
out vec3 normal;
out vec3 fragPos;
out vec2 texCoordinates;
out vec3 tanFragPos;
out vec3 tanViewPos;
out mat3 TBN;

void main() 
{
    //calculate TBN matrix
    vec3 norm = ((inverseNormals) ? -1 : 1) * in_normal;
    mat3 normalMatrix = transpose(inverse(mat3(modelMatrix)));
    vec3 T = normalize(normalMatrix * in_tangent);
    vec3 N = normalize(normalMatrix * norm);
    
    T = normalize(T - dot(T, N) * N);
    vec3 B = cross(N, T);
    TBN = (mat3(T, B, N));
    mat3 TBNt = transpose(TBN);

//normalMatrix = transpose(inverse(mat3(cameraMatrix * modelMatrix)));
	fragPos =  vec3(modelMatrix * vec4(in_Position, 1.0));
	normal = normalMatrix * norm;
	texCoordinates = in_tex_coordinates;
	gl_Position = VC * vec4(fragPos, 1.0);
    tanFragPos = TBN * fragPos;
    tanViewPos = TBN * viewPos;
    //fragPos =  vec3(cameraMatrix * modelMatrix * vec4(in_Position, 1.0));
}