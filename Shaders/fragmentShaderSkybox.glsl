#version 450

/*layout (location = 0)*/ out vec4 fragColor;
//layout (location = 1) out vec4 fragColor2;

in vec3 pos;
in vec3 tex;

uniform samplerCube skybox;

void main() 
{
	fragColor = vec4(pow(texture(skybox, tex).rgb, vec3(2.2)), 1.0);	//with gamma corection
	//fragColor = vec4(1.0, 0.0, 0.0, 1.0);
}
