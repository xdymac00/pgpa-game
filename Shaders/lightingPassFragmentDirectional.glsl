#version 450

// texture type constants (emulate enum)
const uint texTypeDiffuse = 1;
const uint texTypeSpecular = 2;
const uint texTypeAmbient= 3;
const uint texTypeNormal = 4;

#define HEIGHT_SCALE 0.015

//input
in vec2 texCoordinates;

// uniforms

uniform vec3 viewPos;

//ssao
uniform sampler2D ssao;
uniform bool enableSSAO;
// G-buffer
uniform sampler2D gPosition;
uniform sampler2D gNormal;
uniform sampler2D gAlbedoSpec;


// output
/*layout (location = 0)*/ out vec4 fragColor;
//layout (location = 1) out vec4 fragColor2;

// function prototypes
vec3 CalcDirLight(vec3 lDirection, vec3 lColor, vec3 normal, vec3 viewDir, vec2 texCoord, float yPos);

vec3 getTexture(uint texType, vec2 texCoord);

void main() 
{
	vec3 fragPos = texture(gPosition, texCoordinates).rgb;
	vec3 norm = texture(gNormal, texCoordinates).rgb;
	vec3 diffuse = texture(gAlbedoSpec, texCoordinates).rgb;
	float specular = texture(gAlbedoSpec, texCoordinates).a;

	vec3 lColor = /*2.0 **/ vec3(0.9179, 0.042, 0.0507);
	//vec3 lColor = vec3(0.96180892, 0.23670325, 0.25785);

	/*vec3 directions[4] = {
		vec3(0.0, 1.0, 1.0),
		vec3(0.0, 1.0, -1.0),
		vec3(1.0, 1.0, 0.0),
		vec3(-1.0, 1.0, 0.0)
	};*/

	vec3 viewDir = normalize(viewPos - fragPos);

	vec3 result = vec3(0.0, 0.0, 0.0);

//	for (int i = 0; i < 4; i++)
	result += CalcDirLight(vec3(0.0, 1.0, 1.0),  lColor, norm, viewDir, texCoordinates, fragPos.y+1);
	result += CalcDirLight(vec3(0.0, 1.0, -1.0), lColor, norm, viewDir, texCoordinates, fragPos.y+1);
	result += CalcDirLight(vec3(1.0, 1.0, 0.0),  lColor, norm, viewDir, texCoordinates, fragPos.y+1);
	result += CalcDirLight(vec3(-1.0, 1.0, 0.0), lColor, norm, viewDir, texCoordinates, fragPos.y+1);

	fragColor = vec4(result, 1.0); 
}


vec3 getTexture(uint texType, vec2 texCoord) {
	switch (texType) {
	case texTypeDiffuse:
		return texture(gAlbedoSpec, texCoord).rgb;
	case texTypeSpecular:
		float tmp = texture(gAlbedoSpec, texCoord).a;
		return vec3(tmp, tmp, tmp);
	case texTypeAmbient:
		return texture(gAlbedoSpec, texCoord).rgb;
	case texTypeNormal:
		return texture(gNormal, texCoord).rgb;
	}
}

vec3 CalcDirLight(vec3 lDirection, vec3 lColor, vec3 normal, vec3 viewDir, vec2 texCoord, float yPos)
{
	vec3 ambient  = vec3(0);
	vec3 diffuse  = vec3(0);
	vec3 specular = vec3(0);
	float attenuation =  1/( 0.2 + 0.2 * yPos*yPos);
	//float attenuation = 1 / ( 0.07 * yPos * yPos + 0.14 * yPos + 1);

	vec3 lightDir = normalize(-lDirection);

	// diffuse shading
	float diff = max(dot(normal, lightDir), 0.0);
	// specular shading
	vec3 reflectDir = reflect(-lightDir, normal);
	float spec = pow(max(dot(viewDir, reflectDir), 0.0), 16);
	// combine results
	ambient  = lColor * 0.3 * getTexture(texTypeDiffuse, texCoord) * ((enableSSAO) ? texture(ssao, texCoord).r : 1.0);
	diffuse  = lColor * diff *  getTexture(texTypeDiffuse, texCoord);
	specular = lColor * spec *  getTexture(texTypeSpecular, texCoord);
	return ( ambient + attenuation * ( diffuse + specular));
}

