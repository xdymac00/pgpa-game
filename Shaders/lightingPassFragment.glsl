#version 450

// texture type constants (emulate enum)
const uint texTypeDiffuse = 1;
const uint texTypeSpecular = 2;
const uint texTypeAmbient= 3;
const uint texTypeNormal = 4;

#define HEIGHT_SCALE 0.015

//define structures
struct DirectionLight {
	vec3 direction;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;

	bool activ;
};

struct PointLight {
	vec3 position;
	vec3 color;

	float constant;
	float linear;
	float quadratic;
	
	bool activ;
};

// defines for array sizes
#define NR_LIGHTS_DIR 1
#define NR_LIGHTS_POINT 2

//input
in vec2 texCoordinates;
in PointLight pLight;

// uniforms
//uniform PointLight pointLight;
//uniform PointLight		pointLights[NR_LIGHTS_POINT];
uniform vec3 viewPos;
uniform vec2 screenSize;
//ssao
uniform bool enableSSAO;
uniform sampler2D ssao;
// G-buffer
uniform sampler2D gPosition;
uniform sampler2D gNormal;
uniform sampler2D gAlbedoSpec;




// output
layout (location = 0) out vec4 fragColor;
//layout (location = 1) out vec4 fragColor2;

// function prototypes
//vec3 CalcDirLight(DirectionLight light, vec3 normal, vec3 viewDir, vec2 texCoord);
vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir, vec2 texCoord);
vec3 fog(vec3 color, vec3 fragPos);


vec3 getTexture(uint texType, vec2 texCoord);
vec2 paralaxMapping(vec3 viewDir, vec2 texCoords);
float getDepth(vec2 coord);

void main() 
{
	vec2 texCoord = gl_FragCoord.xy / screenSize;

	vec3 fragPos = texture(gPosition, texCoord).rgb;
	vec3 norm = texture(gNormal, texCoord).rgb;
	vec3 diffuse = texture(gAlbedoSpec, texCoord).rgb;
	float specular = texture(gAlbedoSpec, texCoord).a;


	vec3 viewDir = normalize(viewPos - fragPos);

	vec3 result = vec3(0.0, 0.0, 0.0);

	result += CalcPointLight(pLight, norm, fragPos, viewDir, texCoord);
	fragColor = vec4(result, 1.0); // vec4(fog(result, fragPos), 1.0);

}


vec3 getTexture(uint texType, vec2 texCoord) {
	switch (texType) {
	case texTypeDiffuse:
		return texture(gAlbedoSpec, texCoord).rgb;	// with gama corection
	case texTypeSpecular:
		float tmp = texture(gAlbedoSpec, texCoord).a;
		return vec3(tmp, tmp, tmp);
	case texTypeAmbient:
		return texture(gAlbedoSpec, texCoord).rgb;
	case texTypeNormal:
		return texture(gNormal, texCoord).rgb;
	}
}

vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir, vec2 texCoord)
{
	vec3 ambient = vec3(0);
	vec3 diffuse = vec3(0);
	vec3 specular = vec3(0);

	vec3 lightDir = normalize(light.position - fragPos);
	// diffuse shading
	float diff = max(dot(normal, lightDir), 0.0);
	// specular shading
	vec3 reflectDir = reflect(-lightDir, normal);
	float spec = pow(max(dot(viewDir, reflectDir), 0.0), 16);
	// attenuation
	float distance = length(light.position - fragPos);
	//float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));
	float attenuation =  1.0 / ( 1.0 +  light.quadratic *  distance * distance);
	// combine results
	ambient = light.color * 0.3 * getTexture(texTypeAmbient, texCoord) * ((enableSSAO) ? texture(ssao, texCoord).r : 1.0);
	diffuse = light.color * diff *  getTexture(texTypeDiffuse, texCoord);
	specular = light.color * spec * getTexture(texTypeSpecular, texCoord); //vec3(texture(material.specular, texCoordinates));
	return attenuation * (ambient + diffuse + specular);

}
