#version 450

layout(location = 0) in vec3 in_position;
//layout(location = 1) in vec4 in_color;

layout(location = 3) in mat4 modelMatrix;

uniform mat4 projectionMatrix;
uniform mat4 cameraMatrix;
//uniform mat4 modelMatrix;

out vec4 color;

void main() 
{
	color = vec4(1.0, 0.0, 0.0, 1.0);
	gl_Position = projectionMatrix * cameraMatrix * modelMatrix * vec4(in_position, 1.0);
}