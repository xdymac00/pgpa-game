#version 450

//input
in vec2 TexCoords;

// output
out float FragColor;

uniform sampler2D gPosition;
uniform sampler2D gNormal;
uniform sampler2D texNoise;

uniform vec2 screenSize;
uniform vec3 samples[64];
uniform mat4 projection;
uniform mat4 view;
uniform mat4 itView;

int kernelSize = 64;
uniform float radius;
float bias = 0.025;


void main() 
{
	const vec2 noiseScale = screenSize / 4.0;
    // get input for SSAO algorithm
    vec3 fragPos = vec3( view * vec4(texture(gPosition, TexCoords).xyz, 1.0)); // view space
    vec4 n = itView * normalize(vec4(texture2D(gNormal, TexCoords).xyz, 0.0) ); // view space
    vec3 normal = normalize(vec3(n.rgb));
    
    vec3 randomVec = normalize(texture(texNoise, TexCoords * noiseScale).xyz);
    // create TBN change-of-basis matrix: from tangent-space to view-space
    vec3 tangent = normalize(randomVec - normal * dot(randomVec, normal));
    vec3 bitangent = cross(normal, tangent);
    mat3 TBN = mat3(tangent, bitangent, normal);
    // iterate over the sample kernel and calculate occlusion factor
    float occlusion = 0.0;
    for(int i = 0; i < kernelSize; ++i)
    {
        // get sample position
        vec3 sampl = TBN * samples[i]; // from tangent to view-space
        sampl = fragPos + sampl * radius; 
        
        // project sample position (to sample texture) (to get position on screen/texture)
        vec4 offset = vec4(sampl, 1.0);
        offset = projection * offset; // from view to clip-space
        offset.xyz /= offset.w; // perspective divide
        offset.xyz = offset.xyz * 0.5 + 0.5; // transform to range 0.0 - 1.0
        
        // get sample depth
        vec4 samplePos = view *  vec4(texture(gPosition, offset.xy).xyz, 1.0);
        float sampleDepth = samplePos.z / samplePos.w; // get depth value of kernel sample

        
        // range check & accumulate
        float rangeCheck = smoothstep(0.0, 1.0, radius / abs(fragPos.z - sampleDepth));
        occlusion += (sampleDepth >= sampl.z + bias ? 1.0 : 0.0) * rangeCheck;           
    }
    occlusion = 1.0 - (occlusion / kernelSize);
    FragColor =  occlusion;
    //FragColor =  vec4( normalize(vec4(texture2D(gNormal, TexCoords)).rgb), 1.0);
}
