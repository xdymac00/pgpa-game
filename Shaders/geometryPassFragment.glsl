#version 450

// texture type constants (emulate enum)
const uint texTypeDiffuse = 1;
const uint texTypeSpecular = 2;
const uint texTypeAmbient= 3;
const uint texTypeNormal = 4;

#define HEIGHT_SCALE  0.015


struct Material {
	bool useSpecular;
	bool useAmbient;
	bool useNormal;
	bool useDisplacemnet;
	sampler2D diffuse;
	sampler2D specular;
	sampler2D ambient;
	sampler2D normal;
	sampler2D displacement;
	float shininess;
};

//input
in vec3 normal;
in vec3 fragPos;
in vec2 texCoordinates;
in mat3 TBN;
// parameters for paralax mapping
in vec3 tanViewPos;
in vec3 tanFragPos;

uniform float paralaxStr;
uniform bool enableParalax;

uniform Material material;
uniform float texMultiplier;

// outputs
layout (location = 0) out vec3 gPosition;
layout (location = 1) out vec3 gNormal;
layout (location = 2) out vec4 gAlbedo;

// function prototypes
vec3 getTexture(uint texType, vec2 texCoord);
vec2 paralaxMapping(vec3 viewDir, vec2 texCoords);
float getDepth(vec2 coord);

void main() 
{
	vec3 viewDir = normalize(tanViewPos - tanFragPos);
	vec2 texCoord = (enableParalax)?paralaxMapping(viewDir, texCoordinates * texMultiplier):texCoordinates*texMultiplier;
	
	gPosition = fragPos;
	gNormal = normalize(getTexture(texTypeNormal, texCoord));
	gAlbedo.rgb = getTexture(texTypeDiffuse, texCoord);
	gAlbedo.a = getTexture(texTypeDiffuse, texCoord).r;
}

vec2 paralaxMapping(vec3 viewDir, vec2 texCoords) {
	if (!material.useDisplacemnet) return texCoords;
	// number of depth layers
    const float minLayers = 8;
    const float maxLayers = 32;
    float numLayers = mix(maxLayers, minLayers, abs(dot(vec3(0.0, 0.0, 1.0), viewDir)));  
    // calculate the size of each layer
    float layerDepth = 1.0 / numLayers;
    // depth of current layer
    float currentLayerDepth = 0.0;
    // the amount to shift the texture coordinates per layer (from vector P)
    vec2 P = viewDir.xy / viewDir.z * paralaxStr;//HEIGHT_SCALE; 
    vec2 deltaTexCoords = P / numLayers;
  
    // get initial values
    vec2  currentTexCoords     = texCoords;
    float currentDepthMapValue = getDepth(currentTexCoords);
      
    while(currentLayerDepth < currentDepthMapValue)
    {
        // shift texture coordinates along direction of P
        currentTexCoords -= deltaTexCoords;
        // get depthmap value at current texture coordinates
        currentDepthMapValue = getDepth(currentTexCoords);  
        // get depth of next layer
        currentLayerDepth += layerDepth;  
    }
    
    // get texture coordinates before collision (reverse operations)
    vec2 prevTexCoords = currentTexCoords + deltaTexCoords;

    // get depth after and before collision for linear interpolation
    float afterDepth  = currentDepthMapValue - currentLayerDepth;
    float beforeDepth = getDepth(prevTexCoords) - currentLayerDepth + layerDepth;
 
    // interpolation of texture coordinates
    float weight = afterDepth / (afterDepth - beforeDepth);
    vec2 finalTexCoords = prevTexCoords * weight + currentTexCoords * (1.0 - weight);

    return finalTexCoords;

}
float getDepth(vec2 coord) {
	if (material.useDisplacemnet) {
		return texture(material.displacement, coord).r;
	}
	else {
		return 0.0;
	}
}

vec3 getTexture(uint texType, vec2 texCoord) {
	switch (texType) {
	case texTypeDiffuse:
		return pow(vec3(texture(material.diffuse, texCoord)), vec3(2.2)); // gamma corection
		//return vec3(0.2, 0.2, 0.2);
		break;
	case texTypeSpecular:
		if (material.useSpecular) {
			return vec3(texture(material.specular, texCoord));
		}
		else {
			return vec3(texture(material.diffuse, texCoord));
		}
		break;
	case texTypeAmbient:
		if (material.useAmbient) {
			return vec3(texture(material.ambient, texCoord));
		}
		else {
			return vec3(texture(material.diffuse, texCoord));
		}
		break;
	case texTypeNormal:
		if (material.useNormal) {
			vec3 norm = vec3(texture(material.normal, texCoord));
			norm = TBN * normalize(norm*2.0 - 1.0);
			return norm;
		}
		else {
			return normalize(normal);
		}
		break;
	}
}

