#version 450

struct Material {
	bool useSpecular;
	bool useAmbient;
	bool useNormal;
	bool useDisplacemnet;
	sampler2D diffuse;
	sampler2D specular;
	sampler2D ambient;
	sampler2D normal;
	sampler2D displacement;
	float shininess;
};

in vec3 normal;
in vec3 fragPos;
in vec2 texCoordinates;

// outputs
layout (location = 0) out vec3 gPosition;
layout (location = 1) out vec3 gNormal;
layout (location = 2) out vec4 gAlbedo;
//out vec4 fragColor;

uniform Material material;
uniform float iTime;
uniform vec3 viewPos;
uniform float texMultiplier;


// lava shader from https://www.shadertoy.com/view/4lXfR7

// random2 function by Patricio Gonzalez
vec2 random2( vec2 p ) {
    return fract(sin(vec2(dot(p,vec2(127.1,311.7)),dot(p,vec2(269.5,183.3))))*43758.5453);
}

// Value Noise by Inigo Quilez - iq/2013
// https://www.shadertoy.com/view/lsf3WH
float noise(vec2 st) {
    vec2 i = floor(st);
    vec2 f = fract(st);

    vec2 u = f*f*(3.0-2.0*f);

    return mix( mix( dot( random2(i + vec2(0.0,0.0) ), f - vec2(0.0,0.0) ), 
                     dot( random2(i + vec2(1.0,0.0) ), f - vec2(1.0,0.0) ), u.x),
                mix( dot( random2(i + vec2(0.0,1.0) ), f - vec2(0.0,1.0) ), 
                     dot( random2(i + vec2(1.0,1.0) ), f - vec2(1.0,1.0) ), u.x), u.y);
}

vec3 magmaFunc(vec3 color, vec2 uv, float detail, float power,
              float colorMul, float glowRate, bool animate, float noiseAmount)
{
    vec3 rockColor = vec3(0.09 + abs(sin(iTime * 0.75)) * 0.03, 0.02, 0.02);
    float minDistance = 1.;
    uv *= detail;
    
    vec2 cell = floor(uv);
    vec2 frac = fract(uv);
    
    for (int i = -1; i <= 1; i++) {
        for (int j = -1; j <= 1; j++) {
        	vec2 cellDir = vec2(float(i), float(j));
            vec2 randPoint = random2(cell + cellDir);
            randPoint += noise(uv /*+ vec2(sin(iTime))*/) * noiseAmount;
            randPoint = animate ? 0.5 + 0.5 * sin(iTime * 0.35 + 6.2831 * randPoint) : randPoint;
            minDistance = min(minDistance, length(cellDir + randPoint - frac));
        }
    }
    	
    float powAdd = sin(uv.x * 2. + iTime * glowRate) + sin(uv.y * 2.0 + iTime * glowRate);
	vec3 outColor = vec3(color * pow(minDistance, power + powAdd * 0.95) * colorMul);
    outColor.rgb = mix(rockColor, outColor.rgb, minDistance);
    return outColor;
}

vec3 gamma(vec3 color) {
	return pow(color, vec3(2.2));
}

void main()
{    
	vec2 uv = texCoordinates * 40.0;
    //uv.x *= iResolution.x / iResolution.y;
    
	vec4 tex = texture(material.diffuse, uv); 
	tex.rgb = gamma(tex.rgb);
	tex.rgb /= 2;

    vec4 fragColor = vec4(0.0);
    fragColor.rgb += magmaFunc(gamma(vec3(1.5, 0., 0.0)), uv, 3.0,  2.5, 1.15, 1.5, false, 1.5);
    //fragColor.rgb += magmaFunc(vec3(1.5, 1.5, 0.)/*vec3(1.5, .45, 0.)*/, uv , 6.0, 3.0, .4, 1., true, 0.0);
    fragColor.rgb += magmaFunc(gamma(vec3(1.2, .4, 0.0)), uv, 8.0, 4.0, .2, 1.9, true, 0.5);
    fragColor.a = 0.0;// mix(texture(material.specular, uv).r, 0.0, fragColor.r);


    //float brightness = dot(fragColor.rgb, vec3(0.2126, 0.7152, 0.0722));
    gAlbedo.rgb = mix(tex.rgb, fragColor.rgb,fragColor.r);//fragColor + fragColor * tex;
    //gNormal = mix(texture(material.normal, uv).rgb, vec3(1.0, 0.0, 0.0), fragColor.r); // vec3(0.0, 1.0, 0.0);
    //gNormal = mat3(1.0, 0.0, 0.0, 0.0, 0.0, -1.0, 0.0, 1.0, 0.0) * gNormal;
    gNormal = vec3(0.0, 1.0, 0.0);
    gPosition = fragPos;
}