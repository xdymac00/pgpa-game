#version 450

// inputs
layout (location = 0) in vec3 in_Position;
layout (location = 1) in vec2 in_tex_coordinates;

out vec2 TexCoords;

void main()
{
    TexCoords = in_tex_coordinates;
    gl_Position =  vec4(in_Position, 1.0);
}