#version 400

layout(location = 0) in vec3 in_Position;
layout(location = 1) in vec3 in_normal;
layout(location = 2) in vec2 in_tex_coordinates;

uniform mat4 modelMatrix;
uniform mat4 projectionMatrix;
uniform mat4 cameraMatrix;


out	vec3 normal_v;
//	vec3 fragPos_v;
out	vec2 texCoordinates_v;


void main() 
{
//	vs_out.fragPos_v = vec3(modelMatrix * vec4(in_Position, 1.0));
	//vs_out.normal_v = mat3(transpose(inverse(modelMatrix))) * in_normal;

	texCoordinates_v = in_tex_coordinates;
	gl_Position = projectionMatrix * cameraMatrix * modelMatrix * vec4(in_Position, 1.0);
}