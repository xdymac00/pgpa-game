#version 450

// Inputs
layout(location = 0) in vec3 in_Position;
layout(location = 1) in vec3 in_normal;
layout(location = 2) in vec2 in_tex_coordinates;
layout(location = 3) in vec3 in_tangent;


layout(location = 4) in vec3 position;
layout(location = 5) in float scale;
layout(location = 6) in float in_alpha;

// Uniforms
//uniform mat4 projectionMatrix;
//uniform mat4 cameraMatrix;
uniform mat4 VC;
uniform vec3 up;
uniform vec3 right;

// Outputs
out vec3 normal;
out vec3 fragPos;
out vec2 texCoordinates;
out float alpha;

void main() 
{
 //   vec3 right = vec3(cameraMatrix[0][0], cameraMatrix[1][0], cameraMatrix[2][0]);
 //   vec3 up = vec3(cameraMatrix[0][1], cameraMatrix[1][1], cameraMatrix[2][1]);
//normalMatrix = transpose(inverse(mat3(cameraMatrix * modelMatrix)));
	fragPos = position +
        right * in_Position.x * scale +
        up * in_Position.y * scale; 
	texCoordinates = in_tex_coordinates;
	gl_Position = VC * vec4(fragPos, 1.0);
    alpha = in_alpha;
    //fragPos =  vec3(cameraMatrix * modelMatrix * vec4(in_Position, 1.0));
}