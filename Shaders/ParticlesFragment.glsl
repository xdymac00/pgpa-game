#version 450

// texture type constants (emulate enum)
const uint texTypeDiffuse = 1;
const uint texTypeSpecular = 2;
const uint texTypeAmbient= 3;
const uint texTypeNormal = 4;

#define HEIGHT_SCALE  0.015


struct Material {
	bool useSpecular;
	bool useAmbient;
	bool useNormal;
	bool useDisplacemnet;
	sampler2D diffuse;
	sampler2D specular;
	sampler2D ambient;
	sampler2D normal;
	sampler2D displacement;
	float shininess;
};

//input
in vec3 normal;
in vec3 fragPos;
in vec2 texCoordinates;
in float alpha;

uniform Material material;

out vec4 fragColor;

void main() 
{
	vec2 texCoord = texCoordinates;
	
//	gPosition = fragPos;
//	gNormal = normalize(getTexture(texTypeNormal, texCoord));
//	gAlbedo.rgb = getTexture(texTypeDiffuse, texCoord);
//	gAlbedo.a = getTexture(texTypeDiffuse, texCoord).r;
	vec4 colorOut = texture(material.diffuse, texCoord);
	colorOut.rgb = pow(colorOut.rgb, vec3(2.2));
	colorOut.w *= alpha;
	fragColor = colorOut;
}

