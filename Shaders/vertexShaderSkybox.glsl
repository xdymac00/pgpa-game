#version 450

layout(location = 0) in vec3 in_Position;
layout(location = 1) in vec3 in_normal;
layout(location = 2) in vec2 in_tex_coord;

uniform mat4 projectionMatrix;
uniform mat4 cameraMatrix;
uniform mat4 modelMatrix;

out vec3 pos;
out vec3 tex;

void main() 
{
	pos = vec3(in_Position);
	tex = in_Position; // in_tex_coord;
	gl_Position = projectionMatrix * cameraMatrix * modelMatrix * vec4(in_Position, 1.0);
}