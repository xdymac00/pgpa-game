#version 450

out vec4 FragColor;
  
in vec2 texCoordinates;

uniform sampler2D image;
  
uniform int mode; // 0 vertical, 1 horizontal, 2 trasholding
uniform float weight[5] = float[] (0.227027, 0.1945946, 0.1216216, 0.054054, 0.016216);



void main() {
    vec2 tex_offset = 1.0 / textureSize(image, 0); 
    vec3 result = texture(image, texCoordinates).rgb * weight[0];
    if(mode == 1) {
        for(int i = 1; i < 5; ++i) {
            result += texture(image, texCoordinates + vec2(tex_offset.x * i, 0.0)).rgb * weight[i];
            result += texture(image, texCoordinates - vec2(tex_offset.x * i, 0.0)).rgb * weight[i];
        }
    }
    else if(mode == 0) {
        for(int i = 1; i < 5; ++i) {
            result += texture(image, texCoordinates + vec2(0.0, tex_offset.y * i)).rgb * weight[i];
            result += texture(image, texCoordinates - vec2(0.0, tex_offset.y * i)).rgb * weight[i];
        }
    }
    else {
        result = texture(image, texCoordinates).rgb;
        float brightness = dot(result.rgb, vec3(0.2126, 0.7152, 0.0722));
        if(brightness > 0.5)
            result = result.rgb;
        else
            result = vec3(0.0, 0.0, 0.0);
    }
    FragColor = vec4(result.rgb, 1.0);
}