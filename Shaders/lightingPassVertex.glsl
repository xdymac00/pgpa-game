#version 450

struct PointLight {
	vec3 position;
	vec3 color;

	float constant;
	float linear;
	float quadratic;
	
	bool activ;
};

// inputs
layout (location = 0) in vec3 in_Position;
layout (location = 1) in vec2 in_normal;
layout (location = 2) in vec2 in_tex_coordinates;

layout (location = 3) in mat4 lightVolumeModelMatrix;
layout (location = 7) in vec3 lightPosition;
layout (location = 8) in vec3 lightColor;
layout (location = 9) in float lightLinear;
layout (location = 10) in float lightQuadratic;

//uniform mat4 lightVolumeModelMatrix;
uniform mat4 projectionMatrix;
uniform mat4 cameraMatrix;

out vec2 texCoordinates;
out PointLight pLight;

void main()
{
	vec3 fragPos =  vec3(lightVolumeModelMatrix * vec4(in_Position, 1.0));
    texCoordinates = in_tex_coordinates;
    gl_Position = projectionMatrix * cameraMatrix * vec4(fragPos, 1.0);// * lightVolumeModelMatrix /* vec4(in_Position, 1.0)*/;
    //gl_Position =  lightVolumeModelMatrix * vec4( in_Position, 1.0);
   	//gl_Position = projectionMatrix * cameraMatrix * vec4(in_Position, 1);
    pLight.position = lightPosition;
    pLight.color = lightColor;
    pLight.constant = 1.0;
    pLight.linear = lightLinear;
    pLight.quadratic = lightQuadratic;
    pLight.activ = true;
}