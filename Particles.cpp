#include "Particles.h"

Particles::Particles(std::string particleModel)
{
	std::size_t found = particleModel.find_last_of("/\\");
	std::string  path = particleModel.substr(0, found);

	Assimp::Importer importer;
	const aiScene *scene = importer.ReadFile(particleModel, aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_CalcTangentSpace);

	if (!scene) {
		throw std::runtime_error("assimp scene load error");
	}

	processNode(scene->mRootNode, scene, path);

	_particleSystemModelMatrix = glm::translate(_particleSystemModelMatrix, glm::vec3(2.0, 0.0, 2.0));
}

Particles::~Particles()
{
	//glDeleteBuffers(1, &_VAO);
	//glDeleteBuffers(1, &_VBO);
	//glDeleteBuffers(1, &_EBO);
	glDeleteBuffers(1, &_instances);
}

void Particles::createParticles()
{
	for (; _time - _createTime > 0.0; _time -= _createTime) {
		float scale = startScale();
		glm::vec3 velocity = startVelocity();
		for (int i = 0; i < _clusterSize; ++i) {
			_particles.push_back({ startPosition(), scale, startColor(), velocity, startLife() });	// generate particle 
		}
	}
}

void Particles::updateParticles(float time)
{
	_time += time;

	for (Particle &particle : _particles) {
		particle.life -= time;
		particle.scale = nextScale(particle, time); 
		particle.position = nextPos(particle, time);
		particle.velocity = nextVelocity(particle, time);
		particle.alpha = nextColor(particle, time);
	}
	
	_particles.erase(std::remove_if(_particles.begin(), _particles.end(), [](Particle const &particle) { return particle.life < 0.0; }), _particles.end());

	createParticles();

	// Sort particles
	std::sort(_particles.begin(), _particles.end(), [&](Particle &a, Particle &b) { return glm::distance(_camPos, a.position) > glm::distance(_camPos, b.position); });
}


void Particles::processNode(aiNode * node, const aiScene * scene, std::string path)
{
	for (unsigned int i = 0; i < node->mNumMeshes; i++) {
		aiMesh *mesh = scene->mMeshes[node->mMeshes[i]];
		Mesh* procesed = processMesh(mesh, scene, path);
		procesed->init();
#ifdef BOUNDING_VOLUMES
		_boundingVolume.expand(procesed->getBoundingVolume());
#endif //BOUNDING_VOLUMES
		_meshes.push_back(procesed);
		//_meshes.push_back(processMesh(mesh, scene));
		//_meshes.back().init();
	}

	for (unsigned int i = 0; i < node->mNumChildren; i++) {
		processNode(node->mChildren[i], scene, path);
	}


	for (auto &mesh : _meshes) {
		glBindVertexArray(mesh->_VAO);
		glBindBuffer(GL_ARRAY_BUFFER, _instances);

		glEnableVertexAttribArray(4);
		glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), (void*)0);
		glVertexAttribDivisor(4, 1);
		glEnableVertexAttribArray(5);
		glVertexAttribPointer(5, 1, GL_FLOAT, GL_FALSE, sizeof(Particle), (void*)sizeof(glm::vec3));
		glVertexAttribDivisor(5, 1);
		glEnableVertexAttribArray(6);
		glVertexAttribPointer(6, 1, GL_FLOAT, GL_FALSE, sizeof(Particle), (void*)(sizeof(glm::vec3) + sizeof(float)));
		glVertexAttribDivisor(6, 1);

		glBindVertexArray(0);
	}
}

Mesh* Particles::processMesh(aiMesh * meshAi, const aiScene * scene, std::string path)
{
	glGenBuffers(1, &_instances);

	Mesh* mesh = new Mesh();

	//vertices
	for (unsigned int i = 0; i < meshAi->mNumVertices; i++) {
		Vertex vertex = { { 0.0, 0.0, 0.0 },{ 0.0, 0.0, 0.0 },{ 0.0, 0.0 },{ 0.0, 0.0, 0.0 },{ 0.0, 0.0, 0.0, 0.0 } };
		vertex.Position = aiVecToGlmVec(meshAi->mVertices[i]);
		vertex.Normal = aiVecToGlmVec(meshAi->mNormals[i]);
		if (meshAi->mTangents != nullptr)
			vertex.Tangent = aiVecToGlmVec(meshAi->mTangents[i]);
		if (meshAi->mTextureCoords[0])
			vertex.TexCoords = aiVecToGlmVec(meshAi->mTextureCoords[0][i]);
		else
			vertex.TexCoords = glm::vec2(0.0f, 0.0f);

		mesh->pushVertex(vertex);
		//std::cout	<< vertex.Position.x << ", "	<< vertex.Position.y	<< ", " << vertex.Position.z << ", " 
		//<< vertex.Normal.x << ", "		<< vertex.Normal.y		<< ", " << vertex.Normal.z << ", "
		//<< vertex.Tangent.x << ", "		<< vertex.Tangent.y		<< ", " << vertex.Tangent.z << ", "
		//<< vertex.TexCoords.x << ", "	<< vertex.TexCoords.y	<< std::endl
		//;
	}

	//indices
	for (unsigned int i = 0; i < meshAi->mNumFaces; i++) {
		aiFace face = meshAi->mFaces[i];
		for (unsigned int j = 0; j < face.mNumIndices; j++) {
			mesh->pushIndex(face.mIndices[j]);
			//std::cout << face.mIndices[j] << ", ";
		}
	}


	//materials
	Material *material = new Material(scene->mMaterials[meshAi->mMaterialIndex], path);
	mesh->setMaterial(material);

	return mesh;
}

glm::vec3 Particles::aiVecToGlmVec(aiVector3D vectorIn)
{
	return glm::vec3(vectorIn.x, vectorIn.y, vectorIn.z);
}

glm::vec2 Particles::aiVecToGlmVec(aiVector2D vectorIn)
{
	return glm::vec2(vectorIn.x, vectorIn.y);
}

void Particles::drawInstanced(const ShaderProgram * shaderProgram, float time)
{
	int i = 0;
	updateParticles(time);

	glBindBuffer(GL_ARRAY_BUFFER, _instances);
	glBufferData(GL_ARRAY_BUFFER, _particles.size() * sizeof(Particle), _particles.data(), GL_DYNAMIC_DRAW);
	
	for (const auto &mesh : _meshes) {
		mesh->instanceDrawInit(shaderProgram);
		glDrawElementsInstanced(GL_TRIANGLES, mesh->size(), GL_UNSIGNED_INT, 0, /*_instances.size()*/ _particles.size());
		mesh->instanceDrawEnd();
	}
}
