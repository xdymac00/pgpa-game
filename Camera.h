#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#define MOVEMET_SPEED 2.5

enum Camera_Moovment {
	FORWARD,
	BACKWARD,
	LEFT,
	RIGHT,
	UP,
	DOWN
};

enum Camera_Rotation {
	AROUND_X,
	AROUND_Y
};

class Camera
{
public:
	Camera();
	~Camera();

	glm::mat4 getViewMatrix();

	void mooveCamera(Camera_Moovment direction, float deltaTime);
	void rotateCamera(Camera_Rotation axis, float offset);

	void Camera::changePosition(glm::vec3 position, glm::vec3 front);

	glm::vec3 getCameraPosition() const;
	glm::vec3 getCameraDirection() const;

private:
	void updateVectors();

	glm::vec3 _position;
	glm::vec3 _front;
	glm::vec3 _right;
	glm::vec3 _up;
	glm::vec3 _worldUp;
	//float _rotationX = 0.0;
	//float _rotationY = -180.0;
};

