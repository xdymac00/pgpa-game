#include <glad\glad.h>
#include <GLFW\glfw3.h>

#include <iostream>

#include "Renderer.h"

#define WIDTH 1366
#define HEIGHT 768



//TODO remove
extern "C" {
	_declspec(dllexport) DWORD NvOptimusEnablement = 0x00000001;
}

int main(int argc, char* argv[]) {
	Renderer renderer(WIDTH, HEIGHT);

	if (renderer.initialize() == FALSE)
	{
		std::cout << "Failed renderer initialisation" << std::endl;
		glfwTerminate();
		return EXIT_FAILURE;
	}

	//loop
	renderer.mainLoop();

	glfwTerminate();

	return EXIT_SUCCESS;
}