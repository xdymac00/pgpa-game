#include "BoundingBox.h"

#ifdef DRAW_BOUNDING_BOXES
//static init
const GLuint BoundingBox::_indices[16] = {
	0, 1, 2, 3,
	4, 5, 6, 7,
	0, 4, 1, 5, 2, 6, 3, 7
};
#endif // DRAW_BOUNDING_BOXES

BoundingBox::BoundingBox()
{
}


BoundingBox::~BoundingBox()
{
#ifdef DRAW_BOUNDING_BOXES
	glDeleteBuffers(1, &_VAO);
	glDeleteBuffers(1, &_VBO);
	glDeleteBuffers(1, &_EBO);
#endif //DRAW_BOUNDING_BOXES
}

bool BoundingBox::expand(const glm::vec3 point)
{
	bool change = false;
	if (point.x < _minX) { _minX = point.x; change = true; }
	if (point.y < _minY) { _minY = point.y; change = true; }
	if (point.z < _minZ) { _minZ = point.z; change = true; }
	if (point.x > _maxX) { _maxX = point.x; change = true; }
	if (point.y > _maxY) { _maxY = point.y; change = true; }
	if (point.z > _maxZ) { _maxZ = point.z; change = true; }
	return change;
}

bool BoundingBox::expand(const Vertex vertex)
{
	return expand(vertex.Position);
}

bool BoundingBox::expand(const BoundingBox *boundingBox)
{
	bool a = expand(boundingBox->getMinPoints());
	bool b = expand(boundingBox->getMaxPoints());
	return a || b;
}

void BoundingBox::resetValues()
{
	_minX = FLT_MAX;
	_maxX = -FLT_MAX;
	_minY = FLT_MAX;
	_maxY = -FLT_MAX;
	_minZ = FLT_MAX;
	_maxZ = -FLT_MAX;
}

#ifdef DRAW_BOUNDING_BOXES
void BoundingBox::draw(const ShaderProgram* shaderProgram)
{
	if (!_initializedForDraw) { std::cout << "attempt to draw without initialisation." << std::endl; return; }

	glBindVertexArray(_VAO);

	glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, (void*) 0);
	glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_INT, (void*)(4 * sizeof(GLuint)));
	glDrawElements(GL_LINES,     8, GL_UNSIGNED_INT, (void*)(8 * sizeof(GLuint)));

	glBindVertexArray(0);
}

void BoundingBox::initForDraw(/*const ShaderProgram* shaderProgram*/)
{
	calculateBoundingBoxGeometry();

	glGenVertexArrays(1, &_VAO);
	glGenBuffers(1, &_VBO);
	glGenBuffers(1, &_EBO);

	glBindVertexArray(_VAO);
	glBindBuffer(GL_ARRAY_BUFFER, _VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(_vertices), &_vertices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(_indices), &_indices[0], GL_STATIC_DRAW);

	//GLuint positionAttrib = glGetAttribLocation(shaderProgram->getShaderProgram(), "in_position");
	glVertexAttribPointer(/*positionAttrib*/ 0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Position));
	glEnableVertexAttribArray(/*positionAttrib*/ 0);

	//GLuint colorAttrib = glGetAttribLocation(shaderProgram->getShaderProgram(), "in_color");
	glVertexAttribPointer(/*colorAttrib*/ 1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Color));
	glEnableVertexAttribArray(/*colorAttrib*/ 1);

	glBindVertexArray(0);

	_initializedForDraw = true;
}

void BoundingBox::updateBuffers()
{
	calculateBoundingBoxGeometry();

	glBindVertexArray(_VAO);
	glBindBuffer(GL_ARRAY_BUFFER, _VBO);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(_vertices), &_vertices[0]);
	glBindVertexArray(0);
}

void BoundingBox::setColor(const glm::vec4 value)
{
	_color = value;
}
#endif //DRAW_BOUNDINX_BOXES

bool BoundingBox::axisAlignedColision(const BoundingBox* boundingBox)
{
	bool x = (boundingBox->_maxX >= _minX && _maxX >= boundingBox->_minX);	//X-colision
	bool y = (boundingBox->_maxY >= _minY && _maxY >= boundingBox->_minY);	//Y-colision
	bool z = (boundingBox->_maxZ >= _minZ && _maxZ >= boundingBox->_minZ);	//Z-colision
	
	return x && y && z;
}

bool BoundingBox::isPointAbove(glm::vec3 point) const 
{
	return point.y > _maxY;
}

glm::vec3 BoundingBox::getMinPoints() const
{
	return glm::vec3(_minX, _minY, _minZ);
}

glm::vec3 BoundingBox::getMaxPoints() const
{
	return glm::vec3(_maxX, _maxY, _maxZ);
}

#ifdef DRAW_BOUNDING_BOXES
void BoundingBox::calculateBoundingBoxGeometry()
{
	_vertices[0].Position = glm::vec3(_minX, _minY, _minZ);
	_vertices[1].Position = glm::vec3(_maxX, _minY, _minZ);
	_vertices[2].Position = glm::vec3(_maxX, _maxY, _minZ);
	_vertices[3].Position = glm::vec3(_minX, _maxY, _minZ);
	_vertices[4].Position = glm::vec3(_minX, _minY, _maxZ);
	_vertices[5].Position = glm::vec3(_maxX, _minY, _maxZ);
	_vertices[6].Position = glm::vec3(_maxX, _maxY, _maxZ);
	_vertices[7].Position = glm::vec3(_minX, _maxY, _maxZ);

	for (auto &vertex : _vertices) {
		vertex.Color = _color;
	}
}
#endif //DRAW_BOUNDINX_BOXES