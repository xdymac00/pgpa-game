#include "Light.h"

Light::Light()
{
}


Light::~Light()
{
}

void Light::useLight(const ShaderProgram* shaderProgram, const int lightID) const
{
	std::string prefix = getClassPrefix() + "[" + std::to_string(lightID) + "].";
	//shaderProgram->setUniformVariable( prefix + "ambient", getAmbientStr());
	//shaderProgram->setUniformVariable( prefix + "diffuse", getDiffuseStr());
	//shaderProgram->setUniformVariable( prefix + "specular",getSpecularStr());
	shaderProgram->setUniformVariable( prefix + "color", getSpecularStr());
	shaderProgram->setUniformVariable(prefix + "activ", true);
}

glm::vec3 Light::getAmbientStr() const
{
	return _ambientStr;
}

glm::vec3 Light::getDiffuseStr() const
{
	return _diffuseStr;
}

glm::vec3 Light::getSpecularStr() const
{
	return _specularStr;
}

void Light::setAmbientStr(const glm::vec3 str)
{
	_ambientStr = str;
}

void Light::setDiffuseStr(const glm::vec3 str)
{
	_diffuseStr = str;
}

void Light::setSpecularStr(const glm::vec3 str)
{
	_specularStr = str;
}

std::string Light::getClassPrefix() const
{
	return _classPrefix;
}


