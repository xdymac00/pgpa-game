#pragma once


// incomplete !!!!


#include <vector>

#include "DebugDefines.h"
#include "ShaderProgram.h"

class HalfSpace
{
public:
	HalfSpace(glm::vec3 point, glm::vec3 normal);
	~HalfSpace();

	bool colideWith(glm::vec3 point) const;

	void setPoint(const glm::vec3 value);

	static glm::vec3 calculate3HalfSpaceIntersection(const HalfSpace * hs1, const HalfSpace * hs2, const HalfSpace * hs3);
private:
	glm::vec3 _normal;
	glm::vec3 _point;
};

class BoundingVolume
{
public:
	BoundingVolume();
	~BoundingVolume();

	void expand(const glm::vec3 point);
	void expand(const BoundingVolume* boundingVolume);
	void calculateVertices();

	bool colideWith(BoundingVolume* boundryVolume) const;

	void transform(glm::mat4 transformations, bool aplyOnTransformed = false);

#ifdef DRAW_BOUNDING_BOXES
	void draw(ShaderProgram* shaderProgram) const;
#endif // DRAW_BOUNDING_BOXES

private:
	bool isVertexIn(glm::vec3 vertex) const;

	std::vector<HalfSpace> _halfSpaces;
	std::vector<glm::vec3>	_verticesOriginal;
	std::vector<glm::vec3>	_verticesTransformed;

};

