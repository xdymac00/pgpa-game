#include "Light.h"

PointLight::PointLight() 
{
	// setup plane VAO
	_lightVolumeModelMatrix = glm::mat4();
	_lightVolumeModelMatrix[0][0] = 1;
	_lightVolumeModelMatrix[1][1] = 1;
	_lightVolumeModelMatrix[2][2] = 1;
	_lightVolumeModelMatrix[3][3] = 1;
	//_lightVolumeModelMatrix = glm::scale(_lightVolumeModelMatrix, glm::vec3(2.0, 2.0, 2.0));

/*	glGenVertexArrays(1, &_cubeVAO);
	glGenBuffers(1, &_cubeVBO);
	glBindVertexArray(_cubeVAO);
	glBindBuffer(GL_ARRAY_BUFFER, _cubeVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(_cubeVertices), &_cubeVertices, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
	*/
	
}

PointLight::~PointLight()
{
/*	glDeleteBuffers(1, &_cubeVAO);
	glDeleteBuffers(1, &_cubeVBO);*/
}

void PointLight::useLight(const ShaderProgram* shaderProgram, const int lightID) const
{
	Light::useLight(shaderProgram, lightID);
	std::string prefix = getClassPrefix() + "[" + std::to_string(lightID) + "].";
	shaderProgram->setUniformVariable(prefix + "position", _position);

	shaderProgram->setUniformVariable(prefix + "constant", _constant);
	shaderProgram->setUniformVariable(prefix + "linear", _linear);
	shaderProgram->setUniformVariable(prefix + "quadratic", _quadratic);
/*
	glBindVertexArray(_cubeVAO);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 36);
	glBindVertexArray(0);
 */
/*
	glBindVertexArray(_cubeVAO);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);*/
	/*
	std::string prefix = "pointLight.";
	shaderProgram->setUniformVariable(prefix + "ambient", getAmbientStr());
	shaderProgram->setUniformVariable(prefix + "diffuse", getDiffuseStr());
	shaderProgram->setUniformVariable(prefix + "specular", getSpecularStr());
	shaderProgram->setUniformVariable(prefix + "activ", true);
	shaderProgram->setUniformVariable(prefix + "position", _position);

	shaderProgram->setUniformVariable(prefix + "constant", _constant);
	shaderProgram->setUniformVariable(prefix + "linear", _linear);
	shaderProgram->setUniformVariable(prefix + "quadratic", _quadratic);
*/
}	

void PointLight::setConstant(const float value)
{
	_constant = value;
}

void PointLight::setLinear(const float value)
{
	_linear = value;
}

void PointLight::setQuadratic(const float value)
{
	_quadratic = value;
}

void PointLight::setPosition(const glm::vec3 value)
{
	_position = value;
	_lightVolumeModelMatrix = glm::mat4(1.0);
	_lightVolumeModelMatrix = glm::translate(_lightVolumeModelMatrix, value);
}

float PointLight::getRadius()
{
	float _lightMax = std::fmaxf(_diffuseStr.r, _diffuseStr.g);
	//return (-_linear + std::sqrtf(_linear * _linear - 4 * _quadratic * (_constant - (256.0 / 5.0) * _lightMax))) / (2 * _quadratic);
	return std::sqrtf((256.0 * _lightMax - 1) / _quadratic);
}

std::string PointLight::getClassPrefix() const
{
	return _classPrefix;
}


const std::vector<float> PointLight::_cubeVertices = {
		// back face
		-1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f, // bottom-left
		1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f, // top-right
		1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 0.0f, // bottom-right         
		1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f, // top-right
		-1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f, // bottom-left
		-1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 1.0f, // top-left
		// front face
		-1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, // bottom-left
		1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 0.0f, // bottom-right
		1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f, // top-right
		1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f, // top-right
		-1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 1.0f, // top-left
		-1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, // bottom-left
		// left face
		-1.0f,  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-right
		-1.0f,  1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 1.0f, // top-left
		-1.0f, -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-left
		-1.0f, -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-left
		-1.0f, -1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 0.0f, // bottom-right
		-1.0f,  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-right
		// right face
		1.0f,  1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-left
		1.0f, -1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-right
		1.0f,  1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 1.0f, // top-right         
		1.0f, -1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-right
		1.0f,  1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-left
		1.0f, -1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 0.0f, // bottom-left     
		// bottom face
		-1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f, // top-right
		1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 1.0f, // top-left
		1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f, // bottom-left
		1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f, // bottom-left
		-1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 0.0f, // bottom-right
		-1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f, // top-right
		// top face
		-1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, // top-left
		1.0f,  1.0f , 1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f, // bottom-right
		1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 1.0f, // top-right     
		1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f, // bottom-right
		-1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, // top-left
		-1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 0.0f  // bottom-left        
	};
