#pragma once


#include <assimp\Importer.hpp>
#include <assimp\scene.h>
#include <assimp\postprocess.h>

#include <glm\glm.hpp>
//#include <glm\gtc\matrix_transform.hpp>
#include <glad\glad.h>

#include <vector>
#include <string>
#include <functional>
#include <algorithm>


#include "Vertex.h"
#include "Mesh.h"

class Particles
{
public:
	struct Particle
	{
		//glm::mat4 transform;
		glm::vec3 position;
		float scale;
		float alpha;
		glm::vec3 velocity;
		float life;
	};
	
	Particles(std::string particleModel);
	~Particles();

	void createParticles();
	float _createTime = 0.005;
	int _clusterSize = 1;
	float _time = 0;
	void updateParticles(float time);

	int _particlesPerS;

	std::function<glm::vec3(Particle, float)> nextPos = [](Particle particle, float time)->glm::vec3 {return particle.position + particle.velocity * time; };
	std::function<glm::vec3(Particle, float)> nextVelocity = [](Particle particle, float time)->glm::vec3 { return particle.velocity; };
	std::function<float(Particle, float)> nextScale = [](Particle particle, float time)-> float {return particle.scale; };
	std::function<float(Particle, float)> nextColor = [](Particle particle, float time)-> float {return particle.alpha; };
	
	std::function<glm::vec3()> startPosition = [this]() -> glm::vec3 { return _sysPos; };
	std::function<glm::vec3()> startVelocity = []() -> glm::vec3 { return glm::vec3(0.0, 10.0, 0.0); };
	std::function<float()> startColor = []() -> float { return 1.0; };
	std::function<float()> startScale = []() -> float { return 1.0; };
	std::function<float()> startLife= []() -> float { return 1.0; };

	
	void drawInstanced(const ShaderProgram * shaderProgram, float time);
	glm::vec3 _sysPos = glm::vec3(0.0, 0.0, 0.0); // for creating new particles
	glm::vec3 _camPos = glm::vec3(0.0, 0.0, 0.0); // camera position for sorting by distance from camera

private:
	glm::mat4 _particleSystemModelMatrix = glm::mat4();
	std::vector<Particle> _particles;
	std::vector<Mesh*> _meshes;
	unsigned int _VBO, _EBO, _instances, _VAO = 0;

	void processNode(aiNode *node, const aiScene *scene, std::string path);
	Mesh* processMesh(aiMesh *mesh, const aiScene *scene, std::string path);
	glm::vec3 aiVecToGlmVec(aiVector3D vectorIn);
	glm::vec2 aiVecToGlmVec(aiVector2D vectorIn);


/*	const std::vector<const float> _vertices = {
		1, -1, 1, -0, -0, 1, 1, 0, 0, 0.9999, 0.9999,
		0.999999, 1, 1, -0, -0, 1, 1, 0, 0, 0.9999, 0.000100017,
		-1, 1, 1, -0, -0, 1, 1, 0, 0, 0.0001, 0.000100017,
		-1, -1, 1, -0, -0, 1, 1, 0, 0, 0.0001, 0.9999,
		1, 1, -0.999999, -0, 1, 0, 1, 0, 4.76933e-07, 0.9999, 0.00010097,
		-1, 1, -1, -0, 1, 0, 1, 0, -4.47226e-07, 0.0001, 0.000100017,
		-1, 1, 1, -0, 1, 0, 1, 0, 4.76933e-07, 0.0001, 0.9999,
		0.999999, 1, 1, -0, 1, 0, 1, 0, 4.76933e-07, 0.999899, 0.9999,
		1, -1, 1, 0, -1, -0, 0, 0, -1, 0.0001, 0.000100017,
		-1, -1, 1, 0, -1, -0, 0, 0, -1, 0.0001, 0.9999,
		-1, -1, -1, 0, -1, -0, 0, 0, -1, 0.9999, 0.9999,
		1, -1, -1, 0, -1, -0, 0, 0, -1, 0.9999, 0.000100017,
		1, -1, 1, 1, 0, 0, 0, 9.53865e-07, -1, 0.0001, 0.9999,
		1, -1, -1, 1, 0, 0, 0, 0, -1, 0.9999, 0.9999,
		1, 1, -0.999999, 1, 0, 0, 0, 9.53865e-07, -1, 0.9999, 0.00010097,
		0.999999, 1, 1, 1, 0, 0, 0, 9.53865e-07, -1, 0.0001, 0.000100017,
		-1, -1, 1, -1, -0, 0, 0, 0, -1, 0.0001, 0.000100017,
		-1, 1, 1, -1, -0, 0, 0, 0, -1, 0.0001, 0.9999,
		-1, 1, -1, -1, -0, 0, 0, 0, -1, 0.9999, 0.9999,
		-1, -1, -1, -1, -0, 0, 0, 0, -1, 0.9999, 0.000100017,
		-1, -1, -1, 0, 0, -1, 1, 0, 0, 0.0001, 0.000100017,
		-1, 1, -1, 0, 0, -1, 1, 0, 0, 0.0001, 0.9999,
		1, 1, -0.999999, 0, 0, -1, 1, 0, 0, 0.9999, 0.9999,
		1, -1, -1, 0, 0, -1, 1, 0, 0, 0.9999, 0.000100017
	};

	const std::vector<const unsigned int> _indices = {
		0, 1, 2, 0, 2, 3, 4, 5, 6, 4, 6, 7, 8, 9, 10, 8, 10, 11, 12, 13, 14, 12, 14, 15, 16, 17, 18, 16, 18, 19, 20, 21, 22, 20, 22, 23, 1
	};*/
};