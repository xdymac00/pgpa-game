#include "Skybox.h"

#include "stb_image.h"


Skybox::Skybox()
{
	_modelMatrix[0][0] = 1.0;
	_modelMatrix[1][1] = 1.0;
	_modelMatrix[2][2] = 1.0;
	_modelMatrix[3][3] = 1.0;
	_modelMatrix = glm::translate(_modelMatrix, glm::vec3(0.0, -200.0, 0.0));
	_modelMatrix = glm::scale(_modelMatrix, glm::vec3(1000.0, 400.0, 1000.0));
	

	// Compile shaders
	_shaderProgram = new ShaderProgram();
	_shaderProgram->addShader("Shaders/vertexShaderSkybox.glsl", GL_VERTEX_SHADER);
	_shaderProgram->addShader("Shaders/fragmentShaderSkybox.glsl", GL_FRAGMENT_SHADER);
	_shaderProgram->linkProgram();

	// Create cubemap texture
	std::vector<std::string> faces = {
		"Models/skybox/hell_ft.tga",
		"Models/skybox/hell_bk.tga",
		"Models/skybox/hell_up.tga",
		"Models/skybox/hell_dn.tga",
		"Models/skybox/hell_rt.tga",
		"Models/skybox/hell_lf.tga"
	};
	//std::vector<std::string> faces = {
	//	"Models/skybox/Explosionft.jpg",
	//	"Models/skybox/Explosionbk.jpg",
	//	"Models/skybox/Explosionup.jpg",
	//	"Models/skybox/Explosiondn.jpg",
	//	"Models/skybox/Explosionrt.jpg",
	//	"Models/skybox/Explosionlf.jpg"
	//};
	glGenTextures(1, &_skybox);
	//glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, _skybox);
	int width, height, nrChannels;
	unsigned char *data;
	for (GLuint i = 0; i < faces.size(); i++)
	{
		data = stbi_load(faces[i].c_str(), &width, &height, &nrChannels, 0);
		if (data) {
			glTexImage2D(
				GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,
				0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data
			);
		}
		else {
			std::cout << "Cubemap texture failed to load at path: " << faces[i].c_str() << std::endl;
			stbi_image_free(data);
		}
	}
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);


	// Create skybox geometry
	glGenVertexArrays(1, &_VAO);
	glGenBuffers(1, &_VBO);

	glBindVertexArray(_VAO);
	glBindBuffer(GL_ARRAY_BUFFER, _VBO);
	glBufferData(GL_ARRAY_BUFFER, _cubeVertices.size() * sizeof(float), _cubeVertices.data(), GL_STATIC_DRAW);
	
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));

	glBindVertexArray(0);
}


Skybox::~Skybox()
{
	glDeleteBuffers(1, &_VAO);
	glDeleteBuffers(1, &_VBO);
}

void Skybox::draw(glm::mat4 projection, glm::mat4 view)
{
	_shaderProgram->useProgram();
	_shaderProgram->setUniformVariable("projectionMatrix", projection);
	_shaderProgram->setUniformVariable("cameraMatrix", view);
	_shaderProgram->setUniformVariable("modelMatrix", _modelMatrix);

	glDisable(GL_CULL_FACE);
	glBindVertexArray(_VAO);
	//glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, _skybox);
	//_shaderProgram->setUniformVariable("skybox", 0);

	glDrawArrays(GL_TRIANGLES, 0, 36);

	glBindVertexArray(0);
	glEnable(GL_CULL_FACE);
}

ShaderProgram * Skybox::getShader() const
{
	return _shaderProgram;
}
