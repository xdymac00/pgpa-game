#pragma once
#include <glad\glad.h>
#include <GLFW\glfw3.h>

#include <fstream>
#include <iterator>
#include <iostream>

#include <vector>

/// \brief T��da slou�� k na�ten� shaderu ze souboru a n�sledn� ke kompilaci tohoto shaderu
class Shader
{
public:
	Shader(char* shaderPath, GLenum shaderType);
	~Shader();
	std::string loadFile(const char * file);
	void deleteShader();
	GLuint getShader() const;

private:
	GLuint _shader;
};
