#include "Camera.h"



Camera::Camera()
{
	_position = glm::vec3(0.0f, 0.0f, 0.0f);
	_front = glm::vec3(0.0f, 0.0f, -1.0f);
	_up = glm::vec3(0.0f, 1.0f, 0.0f);
	_worldUp = _up;
	_right = glm::normalize(glm::cross(_front, _worldUp));
}


Camera::~Camera()
{
}

glm::mat4 Camera::getViewMatrix()
{
	return glm::lookAt(_position, _position + _front, _up);
}

void Camera::mooveCamera(Camera_Moovment direction, float deltaTime)
{
	float distance = MOVEMET_SPEED * deltaTime;

	switch (direction) {
	case FORWARD:
		_position += _front * distance;
		break;
	case BACKWARD:
		_position -= _front * distance;
		break;
	case LEFT:
		_position -= _right * distance;
		break;
	case RIGHT:
		_position += _right * distance;
		break;
	case UP:
		_position += _worldUp * distance;
		break;
	case DOWN:
		_position -= _worldUp * distance;
		break;
	}
}

void Camera::rotateCamera(Camera_Rotation axis, float offset)
{
	float mouseSensitivity = 0.05; // 0.00002f;
	glm::mat4 mat(0);
	mat[0][0] = 1.0;
	mat[1][1] = 1.0;
	mat[2][2] = 1.0;
	mat[3][3] = 1.0;

	if (axis == AROUND_X) {
		/*_rotationX += (offset * mouseSensitivity);
		_rotationX = (_rotationX > 90.0f) ? 90.0f : _rotationX;
		_rotationX = (_rotationX < -90.0f) ? -90.0f : _rotationX;*/
		//glm::mat4 mat2 = glm::rotate(mat, glm::sin(_front.z), glm::vec3(0.0, 1.0, 0.0));
		//glm::vec4 tmp = mat2 * glm::vec4(1.0, 0.0, 0.0, 1.0);
		mat = glm::rotate(mat, glm::radians(offset * mouseSensitivity),/* glm::vec3(tmp.x/ tmp.w, 0.0, tmp.z / tmp.w)*/glm::vec3(1.0, 0.0, 0.0));	//rotate vector
	}
	else if (axis == AROUND_Y) {
		//_rotationY += (offset * mouseSensitivity);
		mat = glm::rotate(mat, glm::radians(offset * mouseSensitivity), glm::vec3(0.0, 1.0, 0.0)); //rotate vector
	}
	glm::vec4 tmp = mat * glm::vec4(_front, 1.0);
	_front = glm::vec3(tmp.x / tmp.w, tmp.y / tmp.w, tmp.z / tmp.w);

	updateVectors();
}

glm::vec3 Camera::getCameraPosition() const
{
	return _position;
}


glm::vec3 Camera::getCameraDirection() const
{
	return _front;
}

void Camera::changePosition(glm::vec3 position, glm::vec3 front)
{
	_position = position;
	_front = front;
	//_rotationY = glm::degrees(glm::asin(_front.x));
	//_rotationX = glm::degrees(glm::asin(_front.y));
	updateVectors();
}

void Camera::updateVectors()
{
	//_front.x = glm::sin(glm::radians(_rotationY));
	//_front.y = glm::sin(glm::radians(_rotationX));
	//_front.z = glm::cos(glm::radians(_rotationX)) * glm::cos(glm::radians(_rotationY));
	_front = glm::normalize(_front);
	_right = glm::normalize(glm::cross(_front, _worldUp));
	_up = glm::normalize(glm::cross(_right, _front));
}
