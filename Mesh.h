#pragma once

#include <glm/glm.hpp>

#include <vector>

#include "ShaderProgram.h"
#include "Texture.h"
#include "Material.h"
#include "Vertex.h"
#include "BoundingBox.h"
#include "BoundingVolume.h"


class Mesh
{
public:
	Mesh();
	~Mesh();
	void init();
	void draw(const ShaderProgram* shaderProgram) const;
	void instanceDrawInit(const ShaderProgram* shaderProgram) const;
	void instanceDrawEnd() const;
#ifdef DRAW_BOUNDING_BOXES
	void drawBoundingBox(const ShaderProgram* shaderProgram);
#endif //DRAW_BOUNDING_BOXES
	void pushVertex(const Vertex vertex);
	void pushIndex(const unsigned int index);
	void setMaterial(Material* value);

	const BoundingBox* getBoundingBox();
	void setShinines(int value);

	int size();
	GLuint _instances;

#ifdef BOUNDING_VOLUMES
	BoundingVolume* getBoundingVolume();
#endif //BOUNDING_VOLUMES
	GLuint _VAO;
	float _texCoordMultiplier = 1.0;
private:
	GLuint _VBO, _EBO;
	std::vector<unsigned int> _indices;
	Material *_material = nullptr;
	std::vector<Vertex> _vertices;
	
	BoundingBox _boundingBox;
#ifdef BOUNDING_VOLUMES
	BoundingVolume _boundingVolume;
#endif //BOUNDING_VOLUMES
	

	
};

