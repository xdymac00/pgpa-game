#include "Light.h"

DirectionLight::DirectionLight() 
{
}

DirectionLight::~DirectionLight()
{
}

void DirectionLight::useLight(const ShaderProgram* shaderProgram, const int lightID) const
{
	Light::useLight(shaderProgram, lightID);
	std::string prefix = getClassPrefix() + "[" + std::to_string(lightID) + "].";
	shaderProgram->setUniformVariable(prefix + "direction", _direction);

}

void DirectionLight::setDirection(const glm::vec3 value)
{
	_direction = value;
}

void DirectionLight::changeDirection(const glm::vec3 value)
{
	_direction += value;
	std::cout << _direction.x << ", " << _direction.y << ", " << _direction.z << std::endl;
}

std::string DirectionLight::getClassPrefix() const
{
	return _classPrefix;
}
