#include "Shader.h"

Shader::Shader(char * shaderPath, GLenum shaderType)
{
	_shader = glCreateShader(shaderType);
	std::string a = loadFile(shaderPath).c_str();
	char * source = new char[a.length() + 1];
	strcpy(source, a.c_str());

	glShaderSource(_shader, 1,  &source, NULL);
	glCompileShader(_shader);

	delete[] source;

	//osetreni chybovych stavu
	int compileStatus;
	glGetShaderiv(_shader, GL_COMPILE_STATUS, &compileStatus);
	if (compileStatus == GL_FALSE) {		
		GLint maxLength = 0;
		glGetShaderiv(_shader, GL_INFO_LOG_LENGTH, &maxLength);

		std::vector<GLchar> errorLog(maxLength);
		glGetShaderInfoLog(_shader, maxLength, &maxLength, &errorLog[0]);
		std::cout << "SHADER COMPILING ERROR" << std::endl;
		for (auto i : errorLog)
			std::cout << i;
		glDeleteShader(_shader); 

		throw std::runtime_error("shader compilation failed");
	}
}

std::string Shader::loadFile(const char * file)
{
	std::ifstream stream(file);
	if (stream.fail()) {
		std::cout << "ERROR::SHADER::READING_FAILED\n" << file << std::endl;
		throw std::runtime_error(std::string("Can't open \'") + file + "\'");
	}
	return std::string(std::istream_iterator<char>(stream >> std::noskipws), std::istream_iterator<char>());
}

void Shader::deleteShader()
{
	glDeleteShader(_shader);
}

GLuint Shader::getShader() const
{
	return _shader;
}

Shader::~Shader()
{
	glDeleteShader(_shader);
}

