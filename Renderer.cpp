﻿#include "Renderer.h"
#ifdef DEBUG
void GLAPIENTRY
MessageCallback(GLenum source,
	GLenum type,
	GLuint id,
	GLenum severity,
	GLsizei length,
	const GLchar* message,
	const void* userParam)
{
	std::string sType = "";
	switch (type)
	{
	case GL_DEBUG_TYPE_ERROR:
		sType = "ERROR";
		break;
	default:
		sType = "nondefined";
		break;
	}

	std::string sSeverity = "";
	switch (severity)
	{
	case GL_DEBUG_SEVERITY_HIGH:
		sSeverity = "HIGH";
		break;
	case GL_DEBUG_SEVERITY_MEDIUM:
		sSeverity = "MEDIUM";
		break;
	case GL_DEBUG_SEVERITY_LOW:
		sSeverity = "LOW";
		break;
	case GL_DEBUG_SEVERITY_NOTIFICATION:
		sSeverity = "NOTIFICATION";
		break;
	default:
		break;
	}

	std::cerr << "GL CALLBACK: \n\ttype:\t\t" << sType << "\n\tseverity:\t" << sSeverity << "\n\tmessage:\t" << message << std::endl;
}
#endif // DEBUG

Renderer::Renderer(int w, int h)
{
	_width = w;
	_height = h;
}

Renderer::~Renderer()
{

}

bool Renderer::initialize()
{
	//initialising glfw
	if (!glfwInit()) {
		return false;
	}

	//sreating and seting window
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	_window = glfwCreateWindow(_width, _height, WINDOW_NAME, NULL, NULL); // glfwGetPrimaryMonitor() for fullscrean
	if (!_window) {
		std::cout << "Failed window creation" << std::endl;
		glfwTerminate();
		return false;
	}

	glfwMakeContextCurrent(_window);

	glfwSetWindowUserPointer(_window, this);	// ulozeni odkazu na this v windowUserPointeru, vyuzito pri callback funkci

	auto windowSizeChangeCallbackL = [](GLFWwindow* window, int width, int height) { static_cast<Renderer*>(glfwGetWindowUserPointer(window))->framebufferSizeChangeCallback(window, width, height);	 };
	glfwSetFramebufferSizeCallback(_window, windowSizeChangeCallbackL);

	//mouse
	glfwSetInputMode(_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	auto mouseCallback = [](GLFWwindow* window, double a, double b) { static_cast<Renderer*>(glfwGetWindowUserPointer(window))->mouseCallbackM(window, a, b);	};	// funkce pro volani callback funkce. Resi problem pouziti metody jako callback funkce v C API
	glfwSetCursorPosCallback(_window, mouseCallback);

	//glad initialisation	
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return FALSE;
	}

	glViewport(0, 0, _width, _height);

	initGbuffer();
	initHdrBuffer();
	initSSAO();

	// set particle systems
	_particles.push_back(std::make_shared<Particles>("Models/particles/pointUp.obj"));
	_particles.back()->startVelocity =	[]() -> glm::vec3 { return glm::vec3(float(rand()) / RAND_MAX * 40 - 20, float(rand()) / RAND_MAX * 20, float(rand()) / RAND_MAX * 40 - 20); };
	_particles.back()->startScale =		[]() -> float { return float(rand()) / (RAND_MAX * 4) + 0.4; };
	_particles.back()->startLife =		[]() -> float { return 5.0; };
	//_particles.back()->startPosition =	[]() -> glm::vec3 { return glm::vec3(float(rand()) / (RAND_MAX / 600) - 300, 0.0, float(rand()) / (RAND_MAX / 600) - 300); };
	_particles.back()->nextScale =		[](Particles::Particle particle, float time) -> float { return particle.scale / 1.2; };
	_particles.back()->nextVelocity =	[](Particles::Particle particle, float time) -> glm::vec3 { return particle.velocity - glm::vec3(0.0, 30 * time, 0.0); };
	_particles.back()->_clusterSize = 0;

	_particles.push_back(std::make_shared<Particles>("Models/particles/particle.obj"));
	_particles.back()->startVelocity =	[]() -> glm::vec3 { return glm::vec3(float(rand()) / RAND_MAX * 40 - 20, float(rand()) / RAND_MAX * 20, float(rand()) / RAND_MAX * 40 - 20); };
	_particles.back()->startScale =		[]() -> float { return float(rand()) / (RAND_MAX * 4) + 0.4; };
	_particles.back()->startLife =		[]() -> float { return 5.0; };
	_particles.back()->startPosition =	[]() -> glm::vec3 { return glm::vec3(float(rand()) / (RAND_MAX / 600) - 300, 0.0, float(rand()) / (RAND_MAX / 600) - 300); };
	_particles.back()->nextScale =		[](Particles::Particle particle, float time) -> float { return particle.scale / 1.2; };
	_particles.back()->nextVelocity =	[](Particles::Particle particle, float time) -> glm::vec3 { return particle.velocity - glm::vec3(0.0, 30 * time, 0.0); };
	_particles.back()->_clusterSize = 3;

	_particles.push_back(std::make_shared<Particles>("Models/particles/smoke.obj"));
	_particles.back()->startColor =		[]() -> float { return 0.7; };
	_particles.back()->startVelocity =	[]() -> glm::vec3 { return glm::vec3(float(rand()) / RAND_MAX * 5 - 2.5, float(rand()) / RAND_MAX * 5, float(rand()) / RAND_MAX * 5 - 2.5); };
	//_particles.back()->startScale =	[]() -> float { return float(rand()) / (RAND_MAX * 4) + 0.4; };
	_particles.back()->startLife =		[]() -> float { return 5.0; };
	_particles.back()->startPosition =	[]() -> glm::vec3 { return glm::vec3(float(rand()) / (RAND_MAX / 600) - 300, -1.0, float(rand()) / (RAND_MAX / 600) - 300); };
	_particles.back()->nextScale =		[](Particles::Particle particle, float time) -> float { return particle.scale + ( 2.0 * time); };
	_particles.back()->_createTime = 0.01;
	//_particles.back()->nextVelocity =	[](Particles::Particle particle, float time) -> glm::vec3 { return particle.velocity * 0.9f; };
	_particles.back()->nextColor =		[](Particles::Particle particle, float time) -> float { return particle.alpha - 0.7/5 * time; };
	_particles.back()->_clusterSize = 3;

	
	//auto &tmp = _particles.back();
	//_particles.back()->startPosition = [&tmp]() { return tmp->_sysPos; };

	_pointLightsO = std::make_unique<PointLights>();
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
#ifdef DEBUG
	glEnable(GL_DEBUG_OUTPUT);
	glDebugMessageCallback(MessageCallback, 0);
#endif	//DEBUG

#ifdef DEBUG
	_shaderProgramDebugElements = std::make_unique<ShaderProgram>();
	_shaderProgramDebugElements->addShader("Shaders/vertexShaderDebugElements.glsl", GL_VERTEX_SHADER);
	_shaderProgramDebugElements->addShader("Shaders/fragmentShaderDebugElements.glsl", GL_FRAGMENT_SHADER);
	_shaderProgramDebugElements->linkProgram();
#endif // DEBUG
	_shaderProgramGeometryPass = std::make_unique<ShaderProgram>();
	_shaderProgramGeometryPass->addShader("Shaders/geometryPassVertex.glsl", GL_VERTEX_SHADER);
	_shaderProgramGeometryPass->addShader("Shaders/geometryPassFragment.glsl", GL_FRAGMENT_SHADER);
	_shaderProgramGeometryPass->linkProgram();

	_shaderProgramParticleEffects = std::make_unique<ShaderProgram>();
	_shaderProgramParticleEffects->addShader("Shaders/ParticlesVertex.glsl", GL_VERTEX_SHADER);
	_shaderProgramParticleEffects->addShader("Shaders/ParticlesFragment.glsl", GL_FRAGMENT_SHADER);
	_shaderProgramParticleEffects->linkProgram();

	_shaderProgramLightingPass = std::make_unique<ShaderProgram>();
	_shaderProgramLightingPass->addShader("Shaders/lightingPassVertex.glsl", GL_VERTEX_SHADER);
	_shaderProgramLightingPass->addShader("Shaders/lightingPassFragment.glsl", GL_FRAGMENT_SHADER);
	_shaderProgramLightingPass->linkProgram();

	_shaderProgramLightingPassDirectional = std::make_unique<ShaderProgram>();
	_shaderProgramLightingPassDirectional->addShader("Shaders/lightingPassVertexDirectional.glsl", GL_VERTEX_SHADER);
	_shaderProgramLightingPassDirectional->addShader("Shaders/lightingPassFragmentDirectional.glsl", GL_FRAGMENT_SHADER);
	_shaderProgramLightingPassDirectional->linkProgram();

	_shaderProgramLava = std::make_unique<ShaderProgram>();
	_shaderProgramLava->addShader("Shaders/vertexShaderLava.glsl", GL_VERTEX_SHADER);
	_shaderProgramLava->addShader("Shaders/fragmentShaderLava.glsl", GL_FRAGMENT_SHADER);
	_shaderProgramLava->linkProgram();

	_shaderProgramPostProcess = std::make_unique<ShaderProgram>();
	_shaderProgramPostProcess->addShader("Shaders/vertexShaderPostProcess.glsl", GL_VERTEX_SHADER);
	_shaderProgramPostProcess->addShader("Shaders/fragmentShaderPostProcess.glsl", GL_FRAGMENT_SHADER);
	_shaderProgramPostProcess->linkProgram();

	_shaderProgramBloom = std::make_unique<ShaderProgram>();
	_shaderProgramBloom->addShader("Shaders/vertexShaderBloom.glsl", GL_VERTEX_SHADER);
	_shaderProgramBloom->addShader("Shaders/fragmentShaderBloom.glsl", GL_FRAGMENT_SHADER);
	_shaderProgramBloom->linkProgram();

	_shaderProgramSSAO = std::make_unique<ShaderProgram>();
	_shaderProgramSSAO->addShader("Shaders/vertexShaderSSAO.glsl", GL_VERTEX_SHADER);
	_shaderProgramSSAO->addShader("Shaders/fragmentShaderSSAO.glsl", GL_FRAGMENT_SHADER);
	_shaderProgramSSAO->linkProgram();

	_shaderProgramSSAOblur = std::make_unique<ShaderProgram>();
	_shaderProgramSSAOblur->addShader("Shaders/vertexShaderSSAOblur.glsl", GL_VERTEX_SHADER);
	_shaderProgramSSAOblur->addShader("Shaders/fragmentShaderSSAOblur.glsl", GL_FRAGMENT_SHADER);
	_shaderProgramSSAOblur->linkProgram();



	_shaderProgramScore = std::make_unique<ShaderProgram>();
	_shaderProgramScore->addShader("Shaders/vertexShaderScore.glsl", GL_VERTEX_SHADER);
	_shaderProgramScore->addShader("Shaders/fragmentShaderScore.glsl", GL_FRAGMENT_SHADER);
	_shaderProgramScore->linkProgram();
	/*
	_shaderProgramSkybox = std::make_unique<ShaderProgram>();
	_shaderProgramSkybox->addShader("Shaders/vertexShaderSkybox.glsl", GL_VERTEX_SHADER);
	_shaderProgramSkybox->addShader("Shaders/fragmentShaderSkybox.glsl", GL_FRAGMENT_SHADER);
	_shaderProgramSkybox->linkProgram();*/

	//_shaderProgram = std::make_unique<ShaderProgram>();
	//_shaderProgram->addShader("Shaders/vertexShader.glsl", GL_VERTEX_SHADER);
	//_shaderProgram->addShader("Shaders/geometryShader.glsl", GL_GEOMETRY_SHADER);
	//_shaderProgram->addShader("Shaders/fragmentShader.glsl", GL_FRAGMENT_SHADER);
	//_shaderProgram->linkProgram();
	//_shaderProgram->useProgram();

	//camera
	_camera = Camera();
	
#ifdef WIREFRAME_RENDERING
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
#endif // WIREFRAME_RENDERING

	return true;
}

unsigned int quadVAO = 0;
unsigned int quadVBO;
void renderQuad()
{
	if (quadVAO == 0)
	{
		//lightVolume = glm::translate(lightVolume, glm::vec3(0.0, -1.0, 0.0));
		float quadVertices[] = {
			// positions        // texture Coords
			-1.0f,  1.0f, 0.0f, 0.0f, 1.0f,
			-1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
			1.0f,  1.0f, 0.0f, 1.0f, 1.0f,
			1.0f, -1.0f, 0.0f, 1.0f, 0.0f
		};

		glGenVertexArrays(1, &quadVAO);
		glGenBuffers(1, &quadVBO);
		glBindVertexArray(quadVAO);
		glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
	}

	glDisable(GL_CULL_FACE);
	glBindVertexArray(quadVAO);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	glBindVertexArray(0);
	glEnable(GL_CULL_FACE);
}

void Renderer::mainLoop()
{
	createScene();

	//measurements = { 0.0, 0.0, 0.0 };

	while (!glfwWindowShouldClose(_window))
	{
		
		frametimeLogic();
		processPlayer();
		//debugCameraControl();

		//update objects: animations / instanceMatrixes
		for (auto &object : _gameObjects) {
			object->animate(_lastFrameTime);
			//object->updateInstanceMatrix();
		}
		clearGameObjects();

		//colision example
		/*bool colision = false;
		for (auto &gameObject : _gameObjects) {
			if (gameObject == _gameObjects.at(0)) continue;
			if (gameObject != _gameObjects.at(4)) {
				colision |= _gameObjects.at(4)->colide(gameObject);
			}
		}
		if (colision == false)
			_gameObjects.at(4)->translate(glm::vec3(0.0, -_deltaFrameTime/3, 0.0));
		*/

		//light in player position
		//_pointLights.at(0)->setPosition(std::static_pointer_cast<GameObjectPlayer>(_gameObjects.at(PLAYER))->getPosition() + glm::vec3(0.0, 2.0, 0.0));
		//_pointLights.at(0)->setPosition(_camera.getCameraPosition() + glm::vec3(0.0, 0.0, 0.0));

		//transformation matrix
		glm::mat4 projectionMatrix = glm::perspective(glm::radians(45.0f), (float)_width / (float)_height, 0.1f, 2000.0f);
		glm::mat4 camera = _camera.getViewMatrix();

		// render
		// geometry pass, rendering to G-buffer
		//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		//glFinish();
		glEnable(GL_DEPTH_TEST);
		glDepthMask(GL_TRUE);
		glBindFramebuffer(GL_FRAMEBUFFER, _gBuffer);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		//rendering
		_shaderProgramGeometryPass->useProgram();
		//put matrixes to shader 
		//_shaderProgramGeometryPass->setUniformVariable("projectionMatrix", projectionMatrix);
		//_shaderProgramGeometryPass->setUniformVariable("cameraMatrix", camera);
		_shaderProgramGeometryPass->setUniformVariable("VC", projectionMatrix * camera);
		_shaderProgramGeometryPass->setUniformVariable("viewPos", _camera.getCameraPosition());
		_shaderProgramGeometryPass->setUniformVariable("enableParalax", _enableParalax);
		_shaderProgramGeometryPass->setUniformVariable("paralaxStr", _paralaxStr);
		
		for (const auto &model : _models) {
			model->drawInstanced(_shaderProgramGeometryPass.get());
		}

		//// floor
		_shaderProgramLava->useProgram();
		_shaderProgramLava->setUniformVariable("texMultiplier", 200.0f);
		_shaderProgramLava->setUniformVariable("iTime", (float)_lastFrameTime / 1.0f);
		_shaderProgramLava->setUniformVariable("projectionMatrix", projectionMatrix);
		_shaderProgramLava->setUniformVariable("cameraMatrix", camera);
		//_shaderProgramLava->setUniformVariable("frameTime", tmpi * 2);
		//_shaderProgramLava->setUniformVariable("viewPos", _camera.getCameraPosition());
		_floor->draw(_shaderProgramLava.get());
		//glDisable(GL_BLEND);

		// SSAO
		glDepthMask(GL_FALSE);
		glDisable(GL_DEPTH_TEST);
		if (_enableSSAO) {
			glBindFramebuffer(GL_FRAMEBUFFER, _ssaoFrameBuffer);
			
			glClear(GL_COLOR_BUFFER_BIT);

			_shaderProgramSSAO->useProgram();
			_shaderProgramSSAO->setUniformVariable("gPosition", 0);
			_shaderProgramSSAO->setUniformVariable("gNormal", 1);
			_shaderProgramSSAO->setUniformVariable("texNoise", 7);
			_shaderProgramSSAO->setUniformVariable("screenSize", glm::vec2(_width, _height));
			_shaderProgramSSAO->setUniformVariable("samples", _ssaoKernel);	
			_shaderProgramSSAO->setUniformVariable("view", camera);
			_shaderProgramSSAO->setUniformVariable("itView", glm::transpose(glm::inverse(camera)));
			_shaderProgramSSAO->setUniformVariable("projection", projectionMatrix);
			_shaderProgramSSAO->setUniformVariable("radius", _ssaoRadius);
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, _gPosition);
			glActiveTexture(GL_TEXTURE1);
			glBindTexture(GL_TEXTURE_2D, _gNormal);
			glActiveTexture(GL_TEXTURE7);
			glBindTexture(GL_TEXTURE_2D, _ssaoNoiseTexture);
			renderQuad();

			// blur step
			glBindFramebuffer(GL_FRAMEBUFFER, _ssaoBlurFramebuffer);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			_shaderProgramSSAOblur->useProgram();
			_shaderProgramSSAOblur->setUniformVariable("ssaoInput", 8);
			glActiveTexture(GL_TEXTURE8);
			glBindTexture(GL_TEXTURE_2D, _ssaoColorBuffer);
			renderQuad();

			glActiveTexture(GL_TEXTURE9);
			glBindTexture(GL_TEXTURE_2D, _ssaoColorBuffer);
		}
		
		// lighting pass
		glBindFramebuffer(GL_FRAMEBUFFER, _hdrFrameBuffer);	// bind hdr frame buffer
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		_shaderProgramLightingPass->useProgram();
		// Set uniforms
		_shaderProgramLightingPass->setUniformVariable("gPosition", 0);
		_shaderProgramLightingPass->setUniformVariable("gNormal", 1);
		_shaderProgramLightingPass->setUniformVariable("gAlbedoSpec", 2);
		_shaderProgramLightingPass->setUniformVariable("ssao", 9);
		_shaderProgramLightingPass->setUniformVariable("projectionMatrix", projectionMatrix);
		_shaderProgramLightingPass->setUniformVariable("cameraMatrix", camera);
		_shaderProgramLightingPass->setUniformVariable("viewPos", _camera.getCameraPosition());
		_shaderProgramLightingPass->setUniformVariable("screenSize", glm::vec2(_width, _height));
		_shaderProgramLightingPass->setUniformVariable("enableSSAO", _enableSSAO);
		// set openGL for light pass
		glEnable(GL_BLEND);
		//glDepthMask(GL_FALSE);
		//glDisable(GL_DEPTH_TEST);
		glCullFace(GL_FRONT);
		glBlendEquation(GL_FUNC_ADD);
		glBlendFunc(GL_ONE, GL_ONE);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, _gPosition);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, _gNormal);
		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, _gAlbedoSpec);
		glActiveTexture(GL_TEXTURE8);
		glBindTexture(GL_TEXTURE_2D, _ssaoColorBuffer);
		//glBindFramebuffer(GL_FRAMEBUFFER, _hdrFrameBuffer);
		//unsigned int attachments[2] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
		//glDrawBuffers(2, attachments);
		glClear(GL_COLOR_BUFFER_BIT);
		// Draw point lights wirh light volumes
		if (_enablePointLight)
			_pointLightsO->drawLightVolumes(_shaderProgramLightingPass.get());

		// draw directional lights
		_shaderProgramLightingPassDirectional->useProgram();
		_shaderProgramLightingPassDirectional->setUniformVariable("gPosition", 0);
		_shaderProgramLightingPassDirectional->setUniformVariable("gNormal", 1);
		_shaderProgramLightingPassDirectional->setUniformVariable("gAlbedoSpec", 2);
		_shaderProgramLightingPassDirectional->setUniformVariable("ssao", 9);
		_shaderProgramLightingPassDirectional->setUniformVariable("enableSSAO", _enableSSAO);
		_shaderProgramLightingPassDirectional->setUniformVariable("viewPos", _camera.getCameraPosition());
		//_shaderProgramLightingPassDirectional->setUniformVariable("test", _test);
		
		if (_enableDirLight)
			renderQuad();

		// Set openGL back
		glDisable(GL_BLEND);
		//glDepthMask(GL_TRUE);
		//glEnable(GL_DEPTH_TEST);
		glCullFace(GL_BACK);
		
		//// render light volumes wireframe
		//_shaderProgramDebugElements->useProgram();
		//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		//_shaderProgramDebugElements->setUniformVariable("projectionMatrix", projectionMatrix);
		//_shaderProgramDebugElements->setUniformVariable("cameraMatrix", camera);
		//_pointLightsO->drawLightVolumes(_shaderProgramLightingPass.get());
		//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

		// skybox
		glBindFramebuffer(GL_READ_FRAMEBUFFER, _gBuffer);
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, _hdrFrameBuffer); // write to default framebuffer
		glBlitFramebuffer(0, 0, _width, _height, 0, 0, _width, _height, GL_DEPTH_BUFFER_BIT, GL_NEAREST);
		//glBindFramebuffer(GL_FRAMEBUFFER, _hdrFrameBuffer);
		//glDrawBuffers(2, attachments);
		glBindFramebuffer(GL_FRAMEBUFFER, _hdrFrameBuffer);
		
		glDepthMask(GL_TRUE);
		glEnable(GL_DEPTH_TEST);
		_skybox->draw(projectionMatrix, camera);
		glDepthMask(GL_FALSE);
		glDisable(GL_DEPTH_TEST);


		//// Particle effects draw to geometry
		//glEnable(GL_BLEND);
		//glBlendEquation(GL_FUNC_ADD);
		//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		//glEnable(GL_DEPTH_TEST);
		////glDepthMask(GL_TRUE);
		//_shaderProgramParticleEffects->useProgram();
		//_shaderProgramParticleEffects->setUniformVariable("projectionMatrix", projectionMatrix);
		//_shaderProgramParticleEffects->setUniformVariable("cameraMatrix", camera);
		//_shaderProgramParticleEffects->setUniformVariable("viewPos", _camera.getCameraPosition());

		//_particles.at(1)->_sysPos = std::static_pointer_cast<GameObjectPlayer>(_gameObjects.at(PLAYER))->getPosition() + glm::vec3(0.0, 5.0, 0.0);
		//for (const auto &particle : _particles) {
		//	particle->_camPos = _camera.getCameraPosition();
		//	//particle->_sysPos = std::static_pointer_cast<GameObjectPlayer>(_gameObjects.at(PLAYER))->getPosition() + glm::vec3(0.0, 5.0, 0.0);
		//	particle->drawInstanced(_shaderProgramParticleEffects.get(), _deltaFrameTime);
		//}
		//glDisable(GL_BLEND);
		//glDisable(GL_DEPTH_TEST);
		//glDepthMask(GL_FALSE);

		// bloom
		// tep1: thresholding
		glBindFramebuffer(GL_FRAMEBUFFER, _bloomFrameBuffer[1]);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		if (_bloomEnable) {
			_shaderProgramBloom->useProgram();
			_shaderProgramBloom->setUniformVariable("image", 5);
			_shaderProgramBloom->setUniformVariable("mode", 2);
			glActiveTexture(GL_TEXTURE5);
			glBindTexture(GL_TEXTURE_2D, _hdrCollorBuffer);
			renderQuad();
			// step2: blur
			for (int i = 0; i < 2; i++) {	// final image in frame buffer 0
				glBindFramebuffer(GL_FRAMEBUFFER, _bloomFrameBuffer[i]);	// bind default framebuffer
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
				_shaderProgramBloom->setUniformVariable("mode", i);
				glBindTexture(GL_TEXTURE_2D, _bloomCollorBuffer[(i + 1) % 2]);
				renderQuad();
				//horizontal = !horizontal;
			}
		}

		// tone mapping and post process efects
		glBindFramebuffer(GL_FRAMEBUFFER, 0);	// bind default framebuffer
		glDepthMask(GL_TRUE);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glDepthMask(GL_FALSE);
		_shaderProgramPostProcess->useProgram();
		_shaderProgramPostProcess->setUniformVariable("exposure", _exposure);
		_shaderProgramPostProcess->setUniformVariable("tmEnable", _toneMappingEnable);
		_shaderProgramPostProcess->setUniformVariable("fogEnable", _enableFog);
		_shaderProgramPostProcess->setUniformVariable("cameraPos", _camera.getCameraPosition());
		_shaderProgramPostProcess->setUniformVariable("scene", 5);
		_shaderProgramPostProcess->setUniformVariable("bloom", 6);
		_shaderProgramPostProcess->setUniformVariable("gPosition", 1);
		glActiveTexture(GL_TEXTURE5);
		glBindTexture(GL_TEXTURE_2D, _hdrCollorBuffer);
		glActiveTexture(GL_TEXTURE6);
		glBindTexture(GL_TEXTURE_2D, _bloomCollorBuffer[1]);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, _gPosition);
		renderQuad();


		// Particle effects draw
		if (_enableParticles) {
			//glEnable(GL_DEPTH_TEST);
			//glDepthMask(GL_TRUE);
			glEnable(GL_BLEND);
			glBlendEquation(GL_FUNC_ADD);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glEnable(GL_DEPTH_TEST);

			glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
			glBindFramebuffer(GL_READ_FRAMEBUFFER, _hdrFrameBuffer);
			glBlitFramebuffer(0, 0, _width, _height, 0, 0, _width, _height, GL_DEPTH_BUFFER_BIT, GL_NEAREST);
			glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);


			////// floor
			////glDisable(GL_CULL_FACE);
			//_shaderProgramLava->useProgram();
			//_shaderProgramLava->setUniformVariable("texMultiplier", 200.0f);
			//_shaderProgramLava->setUniformVariable("iTime", (float)_lastFrameTime / 1.0f);
			//_shaderProgramLava->setUniformVariable("projectionMatrix", projectionMatrix);
			//_shaderProgramLava->setUniformVariable("cameraMatrix", camera);
			////_shaderProgramLava->setUniformVariable("frameTime", tmpi * 2);
			////_shaderProgramLava->setUniformVariable("viewPos", _camera.getCameraPosition());
			//_floor->draw(_shaderProgramLava.get());
			////glEnable(GL_CULL_FACE);
			////glDisable(GL_BLEND);

			//glDepthMask(GL_TRUE);
			_shaderProgramParticleEffects->useProgram();
			//_shaderProgramParticleEffects->setUniformVariable("projectionMatrix", );
			_shaderProgramParticleEffects->setUniformVariable("VC", projectionMatrix * camera);
			//_shaderProgramParticleEffects->setUniformVariable("viewPos", _camera.getCameraPosition());
			_shaderProgramParticleEffects->setUniformVariable("right", glm::vec3(camera[0][0], camera[1][0], camera[2][0]));
			_shaderProgramParticleEffects->setUniformVariable("up", glm::vec3(camera[0][1], camera[1][1], camera[2][1]));

			_particles.at(0)->_sysPos = std::static_pointer_cast<GameObjectPlayer>(_gameObjects.at(PLAYER))->getPosition() + glm::vec3(0.0, 0.0, 0.0);
			for (const auto &particle : _particles) {
				particle->_camPos = _camera.getCameraPosition();
				//particle->_sysPos = std::static_pointer_cast<GameObjectPlayer>(_gameObjects.at(PLAYER))->getPosition() + glm::vec3(0.0, 5.0, 0.0);
				particle->drawInstanced(_shaderProgramParticleEffects.get(), _deltaFrameTime);
			}
			glDisable(GL_BLEND);
			glDisable(GL_DEPTH_TEST);
			glDepthMask(GL_FALSE);
		}


		
#ifdef DRAW_BOUNDING_BOXES
		if (!turnBBoff) {
			_shaderProgramDebugElements->useProgram();
			_shaderProgramDebugElements->setUniformVariable("projectionMatrix", projectionMatrix);
			_shaderProgramDebugElements->setUniformVariable("cameraMatrix", camera); 
			for (auto &gameObject : _gameObjects) {
				gameObject->drawBoundingBox(_shaderProgramDebugElements.get());
			}
		}
#endif // DRAW_BOUNDING_BOXES

		if (_displayScore) {
			_shaderProgramScore->useProgram();
			//glEnable(GL_BLEND);
			//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			_shaderProgramScore->setUniformVariable("projectionMatrix", projectionMatrix);
			_shaderProgramScore->setUniformVariable("cameraMatrix", camera);
			for (int i = 0; i < _score.size(); i++) {
				_score.at(i)->resetAnim();
				_score.at(i)->setModelMatrix(/*_camera.getCameraPosition()*/ _gameObjects.at(PLAYER)->getModelMatrix());
				_score.at(i)->rotate(glm::vec3(0.0, 0.0, 1.0), -90);
				_score.at(i)->rotate(glm::vec3(0.0, 1.0, 0.0), 90);
				_score.at(i)->translateAnim(/*_camera.getCameraDirection()*/glm::vec3(3.0, -10.0 + i * 2.0, -5.0));
				int num = std::static_pointer_cast<GameObjectPlayer>(_gameObjects.at(PLAYER))->getScore() / pow(10, i);
				_shaderProgramScore->setUniformVariable("number", num);
				_score.at(i)->draw(_shaderProgramScore.get());
			}
			//glDisable(GL_BLEND);
		}

		//buffer swap
		glfwSwapBuffers(_window);
		glfwPollEvents();

#ifdef DEBUG	//printing OpenGL errors
		/*GLenum err;
		while ((err = glGetError()) != GL_NO_ERROR)
		{
			std::cout << "OpenGL error: " << err << std::endl;
		}
		std::cout << "-------------------" << std::endl;*/
#endif // DEBUG

	}

}


void Renderer::processPlayer()
{
	click(GLFW_KEY_P, [&]() { _exposure *= 2.0; std::cout << "exposure set to: " << _exposure << std::endl; });
	click(GLFW_KEY_O, [&]() { _exposure /= 2.0; std::cout << "exposure set to: " << _exposure << std::endl; });
	click(GLFW_KEY_I, [&]() { _toneMappingEnable = !_toneMappingEnable; std::cout << "Enable tone mapping " << _toneMappingEnable << std::endl; });
	click(GLFW_KEY_B, [&]() { _bloomEnable = !_bloomEnable; std::cout << "Enable bloom: " << _bloomEnable << std::endl; });
	click(GLFW_KEY_L, [&]() { _enableDirLight = !_enableDirLight; std::cout << "Enable direction lights: " << _enableDirLight << std::endl; });
	click(GLFW_KEY_K, [&]() { _enablePointLight = !_enablePointLight; std::cout << "Enable point lights: " << _enablePointLight << std::endl; });
	click(GLFW_KEY_F, [&]() { _enableFog = (_enableFog == 0) ? 1 : 0; std::cout << "Enable fog: " << _enableFog << std::endl; });
	click(GLFW_KEY_Z, [&]() { _enableParalax = (_enableParalax == 0) ? 1 : 0; std::cout << "Enable paralax: " << _enableParalax << std::endl; });
	click(GLFW_KEY_X, [&]() { _paralaxStr -= 0.001; std::cout << "paralax str set to: " << _paralaxStr << std::endl; });
	click(GLFW_KEY_C, [&]() { _paralaxStr += 0.001; std::cout << "paralax str set to: " << _paralaxStr << std::endl; });
	click(GLFW_KEY_U, [&]() { _enableSSAO = (_enableSSAO == 0) ? 1 : 0; std::cout << "Enable ssao: " << _enableSSAO << std::endl; });
	click(GLFW_KEY_Y, [&]() { _enableSSAO = _ssaoRadius += 0.1; std::cout << "ssao radius: " << _ssaoRadius << std::endl; });
	click(GLFW_KEY_T, [&]() { _enableSSAO = _ssaoRadius -= 0.1; std::cout << "ssao radius: " << _ssaoRadius << std::endl; });
	click(GLFW_KEY_R, [&]() { _displayScore = !_displayScore; std::cout << "Display score: " << _displayScore << std::endl; measurements.clear(); });
	click(GLFW_KEY_G, [&]() { _enableParticles = !_enableParticles; std::cout << "Enable Particles: " << _enableParticles << std::endl; });
	click(GLFW_KEY_H, [&]() { std::cout << R"(
Help:
	W, S, A, D - movement
	spacebar - jump
	Q, E - camera adjustment
	P, O, I - HDR controll
	B - toggle bloom
	L - toggle directional lights
	K - toggle point lights
	F - toggle fog
	Z, X, C - paralax mapping controll
	U, Y, T - SSAO controll
	G - toggle particles
	
	)" << std::endl; });
	//click(GLFW_KEY_T, [&]() { _test = (_test == 0) ? 1 : 0; std::cout << "Enable bloom: " << _test << std::endl; });

	
	/*cas framu*/
	float deltaTime = _deltaFrameTime;
	if (glfwGetKey(_window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(_window, true);

	if (glfwGetKey(_window, GLFW_KEY_E) == GLFW_PRESS) {
		_cameraYOffset += 0.1;
	}
	if (glfwGetKey(_window, GLFW_KEY_Q) == GLFW_PRESS) {
		_cameraYOffset -= 0.1;
	}
	
	click(GLFW_KEY_R, [&]() { _displayScore = !_displayScore; std::cout << "Display score: " << _displayScore << std::endl; });

	/*hrac*/
	std::shared_ptr<GameObjectPlayer> player = std::static_pointer_cast<GameObjectPlayer>(_gameObjects.at(PLAYER));

	/* moovment forward / backward */
	float a = 0;
	press(GLFW_KEY_W, [&]() { a =  accelerationCoefficient; player->move(); });
	press(GLFW_KEY_S, [&]() { a = -accelerationCoefficient; });

	//friction
	if (a == 0) {
		if (velocity.x >= 0.0) {
			a = -frictionCoefitient;
		}
		else {
			a = frictionCoefitient;
		}
	}

	movementVector = glm::vec3(0.0, 0.0, 0.0);
	movementVector.z= velocity.x * deltaTime + 0.5 * a * deltaTime * deltaTime;
	velocity.x += a * deltaTime;
	if (velocity.x > maxSpeed) { velocity.x = maxSpeed; }

	/* rotation */
	press(GLFW_KEY_A, [&]() {
		player->rotate(glm::vec3(0.0, 1.0, 0.0), rotationSpeed * deltaTime);
	});
	press(GLFW_KEY_D, [&]() {
		player->rotate(glm::vec3(0.0, 1.0, 0.0), -rotationSpeed * deltaTime);
	});

	/* jump */
	click(GLFW_KEY_SPACE, [&]() {
		if (spacePressedCount < 2) {
			spacePressedCount++;
			//forceUpward = jumpForce;
			velocity.y = jumpForce;
			player->jump();
	}});

	movementVector.y = velocity.y * deltaTime - gravity * deltaTime * deltaTime;
	velocity.y += -gravity * deltaTime;
	//if (velocity.y > maxSpeed) { velocity.y = maxSpeed; }
	//if (velocity.y < -maxSpeed) { velocity.y = -maxSpeed; }

	/*kolize*/
	bool colision = false;


	glm::vec3 nextPos = player->getPosition() + movementVector;

	// colision with floor
	if (nextPos.y < -5.0)
	{
		//postaveni na nulu natvrdo

		spacePressedCount = 0;
		//player->translate(glm::vec3(0.0, 500.0, 0.0));

		velocity = glm::vec2(0.0, 0.0);

		std::cout << "Game Over" << std::endl;
		//_gameObjects.clear();
		_gameObjects.erase(_gameObjects.begin(), _gameObjects.end());
		generateGameBoard();
		return;
	}

	std::for_each(_gameObjects.begin(), _gameObjects.end(), [&](std::shared_ptr<GameObject> gameObject) {
		if (gameObject.get() == player.get() || !player->colide(gameObject.get())) {
			return; 
		}

		// only coliding objects left
		const float playerHeightCompensation = 1.29;//1.8;// 1.29;
		float playerBottomHeight = nextPos.y  + playerHeightCompensation;
		float velocityCompensation = ((velocity.y < 0.0) ? velocity.y : 0.0) * deltaTime;
 		if (playerBottomHeight <= gameObject->getPeakheight() + velocityCompensation - 1.3) {	// vertical colision
			if (velocity.x > 0.0) { // front colision
				movementVector.z = 0.0;
				velocity.x = 0.0;
			}
			else {
				movementVector.z = 0.0;
				velocity.x = 0.0;
			}
		}
		else {	// horizontal colision
			if (velocity.y <= 0.0) {
				movementVector.y -= playerBottomHeight - gameObject->getPeakheight() ;
				velocity.y = 0.0;
			}
			spacePressedCount = 0;
		}
	});

	/*vysledny vektor*/
	player->translate(movementVector);

	/*kamera vzdy za hracem*/
	glm::vec3 newCameraPosition = player->getPosition() - (player->getFront() + glm::vec3(0.0, _cameraYOffset, 0.0))*20.0f;
	glm::vec3 newFront = ((player->getPosition() - newCameraPosition) + player->getFront());
	_camera.changePosition(newCameraPosition, newFront);



#ifdef DRAW_BOUNDING_BOXES
	
	static bool b = true;
	if (glfwGetKey(_window, GLFW_KEY_B) == GLFW_PRESS) {
		if (b == true) {
			b = false;
			turnBBoff = !turnBBoff;
		}
	}
	else {
		b = true;
	}
#endif // DRAW_BOUNDING_BOXES

}

void Renderer::debugCameraControl()
{
	if (glfwGetKey(_window, GLFW_KEY_E) == GLFW_PRESS) {
		//_cameraYOffset += 0.1;
		//std::cout << _cameraYOffset << std::endl;
		_test += 0.001;
		std::cout << _test << std::endl;
	}
	if (glfwGetKey(_window, GLFW_KEY_Q) == GLFW_PRESS) {
		//_cameraYOffset -= 0.1;
		//std::cout << _cameraYOffset << std::endl;
		_test -= 0.001;
		std::cout << _test << std::endl;
	}

	float deltaTime = _deltaFrameTime * 10;
	if (glfwGetKey(_window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(_window, true);

	if (glfwGetKey(_window, GLFW_KEY_W) == GLFW_PRESS) {
		_camera.mooveCamera(FORWARD, deltaTime);
	}
	if (glfwGetKey(_window, GLFW_KEY_S) == GLFW_PRESS) {
		_camera.mooveCamera(BACKWARD, deltaTime);
	}
	if (glfwGetKey(_window, GLFW_KEY_A) == GLFW_PRESS) {
		_camera.mooveCamera(LEFT, deltaTime);
	}
	if (glfwGetKey(_window, GLFW_KEY_D) == GLFW_PRESS) {
		_camera.mooveCamera(RIGHT, deltaTime);
	}
	if (glfwGetKey(_window, GLFW_KEY_SPACE) == GLFW_PRESS) {
		_camera.mooveCamera(UP, deltaTime);
	}
	if (glfwGetKey(_window, GLFW_KEY_X) == GLFW_PRESS) {
		_camera.mooveCamera(DOWN, deltaTime);
	}
}

void Renderer::initGbuffer()
{
	glGenFramebuffers(1, &_gBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, _gBuffer);

	// - position color buffer
	glGenTextures(1, &_gPosition);
	glBindTexture(GL_TEXTURE_2D, _gPosition);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, _width, _height, 0, GL_RGB, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, _gPosition, 0);

	// - normal color buffer
	glGenTextures(1, &_gNormal);
	glBindTexture(GL_TEXTURE_2D, _gNormal);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, _width, _height, 0, GL_RGB, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, _gNormal, 0);

	// - color + specular color buffer
	glGenTextures(1, &_gAlbedoSpec);
	glBindTexture(GL_TEXTURE_2D, _gAlbedoSpec);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, _width, _height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, _gAlbedoSpec, 0);

	// - tell OpenGL which color attachments we'll use (of this framebuffer) for rendering 
	unsigned int attachments[3] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2 };
	glDrawBuffers(3, attachments);

	glGenRenderbuffers(1, &_rboDepth);
	glBindRenderbuffer(GL_RENDERBUFFER, _rboDepth);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, _width, _height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, _rboDepth);
	// finally check if framebuffer is complete
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		std::cout << "Framebuffer not complete!" << std::endl;
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Renderer::initHdrBuffer()
{
	glGenFramebuffers(1, &_hdrFrameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, _hdrFrameBuffer);

	glGenTextures(1, &_hdrCollorBuffer);
	glBindTexture(GL_TEXTURE_2D, _hdrCollorBuffer);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, _width, _height, 0, GL_RGBA, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glGenRenderbuffers(1, &_hdrDepthBuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, _hdrDepthBuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, _width, _height);

	glBindFramebuffer(GL_FRAMEBUFFER, _hdrFrameBuffer);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, _hdrCollorBuffer, 0);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, _hdrDepthBuffer);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		std::cout << "HdrFramebuffer not complete!" << std::endl;
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	// init buffer for bloom efect
	for (int i = 0; i < 2; i++) {
		glGenFramebuffers(1, &_bloomFrameBuffer[i]);
		glBindFramebuffer(GL_FRAMEBUFFER, _bloomFrameBuffer[i]);

		glGenTextures(1, &_bloomCollorBuffer[i]);
		glBindTexture(GL_TEXTURE_2D, _bloomCollorBuffer[i]);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, _width, _height, 0, GL_RGBA, GL_FLOAT, NULL);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glBindFramebuffer(GL_FRAMEBUFFER, _bloomFrameBuffer[i]);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, _bloomCollorBuffer[i], 0);

		if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
			std::cout << "BloomFramebuffer not complete!" << std::endl;
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
}

void Renderer::initSSAO()
{
	std::uniform_real_distribution<GLfloat> randomFloats(0.0, 1.0); // generates random floats between 0.0 and 1.0
	std::default_random_engine generator;
	auto lerp = [](float a, float b, float f) -> float { return a + f * (b - a); };
	for (unsigned int i = 0; i < 64; ++i) {
		glm::vec3 sample(randomFloats(generator) * 2.0 - 1.0, randomFloats(generator) * 2.0 - 1.0, randomFloats(generator));
		sample = glm::normalize(sample);
		sample *= randomFloats(generator);
		float scale = float(i) / 64.0;

		// scale samples s.t. they're more aligned to center of kernel
		scale = lerp(0.1f, 1.0f, scale * scale);
		sample *= scale;
		_ssaoKernel.push_back(sample);
	}

	for (unsigned int i = 0; i < 16; i++)
	{
		glm::vec3 noise(
			randomFloats(generator) * 2.0 - 1.0,
			randomFloats(generator) * 2.0 - 1.0,
			0.0f);
		_ssaoNoise.push_back(noise);
	}
	glGenTextures(1, &_ssaoNoiseTexture);
	glBindTexture(GL_TEXTURE_2D, _ssaoNoiseTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, 4, 4, 0, GL_RGB, GL_FLOAT, &_ssaoNoise[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glGenFramebuffers(1, &_ssaoFrameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, _ssaoFrameBuffer);

	glGenTextures(1, &_ssaoColorBuffer);
	glBindTexture(GL_TEXTURE_2D, _ssaoColorBuffer);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, _width, _height, 0, GL_RGB, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, _ssaoColorBuffer, 0);

	glGenFramebuffers(1, &_ssaoBlurFramebuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, _ssaoBlurFramebuffer);
	glGenTextures(1, &_ssaoBlurColor);
	glBindTexture(GL_TEXTURE_2D, _ssaoBlurColor);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, _width, _height, 0, GL_RGB, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, _ssaoBlurColor, 0);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

float Renderer::generateRandomInt(int low, int high)
{
	return  low + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (high - low)));
}

void Renderer::click(int keyEnum, std::function<void()> event, std::function<void()> elseEvent)
{
	static int c = -1;
	if (glfwGetKey(_window, keyEnum) == GLFW_PRESS) {
		if (c == -1) {
			c = keyEnum;
			event();
		}
	}
	else { 
		if (c == keyEnum) c = -1;
		if (elseEvent != nullptr) elseEvent();
	}
}

void Renderer::press(int keyEnum, std::function<void()> event, std::function<void()> elseEvent)
{
	if (glfwGetKey(_window, keyEnum) == GLFW_PRESS) {
			event();
	}
	else if (elseEvent != nullptr) {
		elseEvent();
	}
}

void Renderer::generateGameBoard()
{
	float randomH;
	float randomX;
	float randomY;
	float alreadyBuild[TOWERCOUNT + 1][2];
	int validPlacement = 0;
	float distance;
	std::srand(std::time(nullptr));
	_pointLights.clear();
	
	/*vyska startovniho bodu*/
	randomH = 2.0;


 //	_skyboxModel = _models.at(0)->createInstance(Model::DEFAULT); //(std::make_shared<GameObject>(_models.at(0)));			//0 //skybox
	//_skyboxModel->translate(glm::vec3(0.0, -400.0, 00.0));
	//_skyboxModel->scale(glm::vec3(500.0, 500.0, 500.0));
	
	_floor = _models.at(1)->createInstance(Model::DEFAULT);		//1	//floor
	_floor->translate(glm::vec3(0.0, -1.0, 0.0));
	_floor->scale(glm::vec3(700.0, 00.0, 700.0));


	/*hrac*/
	_gameObjects.push_back(_models.at(4)->createInstance(Model::PLAYE));
	//_gameObjects.at(2)->scale(glm::vec3(0.5, 0.5, 0.5));
	_gameObjects.at(0)->translate(glm::vec3(0.0, randomH + 8, 0.0));
	std::static_pointer_cast<GameObjectPlayer>(_gameObjects.at(PLAYER))->_pointParticles = _particles.at(0);

	//// DEBUG
	//_debug = _models.at(6)->createInstance(Model::DEFAULT);
	//_debug->translate(glm::vec3(0.0, -1.0, 0.0));
	//_debug->scale(glm::vec3(700.0, 00.0, 700.0));

	//startovaci platofrma
	_gameObjects.push_back(_models.at(2)->createInstance(Model::DEFAULT));
	alreadyBuild[0][0] = 0.0;
	alreadyBuild[0][1] = 0.0;

	_gameObjects.at(1)->translate(glm::vec3(0.0, randomH - 1, 0.0));
	_gameObjects.at(1)->setPeakHeight(randomH * 2);
	_gameObjects.at(1)->scale(glm::vec3(3.0, randomH, 3.0));


	/*generovani zkytku plochy*/

	int i = 1;
	while (i < TOWERCOUNT + 1)
	{
		while (validPlacement == 0)
		{
			/*dimenze hraci plochy*/
			randomX = generateRandomInt(-250, 250);
			randomY = generateRandomInt(-250, 250);

			if (i == 0)
				validPlacement = 1;
			else
			{
				for (int j = 0; j < i; j++)
				{
					distance = sqrt((randomX - alreadyBuild[j][0])*(randomX - alreadyBuild[j][0]) + (randomY - alreadyBuild[j][1])*(randomY - alreadyBuild[j][1]));

					//zmena minimalni mezery
					if (distance < 20)
					{
						validPlacement = 0;
						break;
					}
					else
						validPlacement = 1;
				}
			}
		}

		alreadyBuild[i][0] = randomX;
		alreadyBuild[i][1] = randomY;

		/*nahodna vyska bloku*/
		randomH = generateRandomInt(1, 8);

		/*vytvoreni bloku*/
		int blockType = ((rand() % 3) == 0) ? 2 : 7 ;
		_gameObjects.push_back(_models.at(blockType)->createInstance(Model::DEFAULT));
		_gameObjects.back()->translate(glm::vec3(randomX, randomH - 1, randomY));
		_gameObjects.back()->setPeakHeight(randomH * 2);
		_gameObjects.back()->scale(glm::vec3(generateRandomInt(4, 7), randomH, generateRandomInt(4, 7)));

		/*mince na bloku*/
		std::shared_ptr<GameObjectCoin> coin = std::static_pointer_cast<GameObjectCoin>(_models.at(3)->createInstance(Model::COIN));
		_gameObjects.push_back(coin);
		_gameObjects.back()->translate(glm::vec3(randomX, randomH * 2 + 0.5, randomY));

		coin->_light = (std::make_shared<PointLight>());	// light is deleted when coin is
		coin->_light->setPosition(glm::vec3(randomX, randomH * 2 + 1.0, randomY));
		std::function<float(float)> genColVal = [](float max) -> float { return ((float)rand() / (RAND_MAX)) * max; };
		coin->_light->setDiffuseStr(/*glm::vec3(genColVal(1.0), genColVal(1.0), genColVal(1.0))*/ glm::vec3(0.8, 0.8, 0.0));
		_pointLightsO->addLight(coin->_light);

		validPlacement = 0;
		i++;
	}

	_pointLightsO->clearLights();
	for (auto &light : _pointLights) {
		
	}
}


void Renderer::createScene()
{
	//_skyboxModel = new Skybox();

	//model loading
	_models.push_back(std::make_shared<Model>("Models/20sided.obj"));		//0	//skybox
	_models.push_back(std::make_shared<Model>("Models/floor.obj"));			//1	//floor
	_models.push_back(std::make_shared<Model>("Models/cubeTest.obj"));		//2
	_models.push_back(std::make_shared<Model>("Models/+1.obj"));			//3
	_models.push_back(std::make_shared<Model>("Models/Blob.obj"));			//4  //source: https://www.yobi3d.com/q/3D-model/blob?page=1
	_models.push_back(std::make_shared<Model>("Models/number.obj"));		//5
	_models.push_back(std::make_shared<Model>("Models/crater.obj"));		//6 crater
	_models.push_back(std::make_shared<Model>("Models/lavaCube.obj"));		//7
	//_models.push_back(std::make_shared<Model>("Models/particles/particle.obj"));		//8 test
	//_models.at(1)->_drawInstanced = true;
	_models.at(1)->setTexCoordMultiplier(20.0);
	_models.at(1)->_inverseNormals = true;
	_models.at(2)->_drawInstanced = true;
	_models.at(3)->_drawInstanced = true;
	_models.at(4)->_drawInstanced = true;
	_models.at(6)->_drawInstanced = true;
	_models.at(7)->_drawInstanced = true;

	_models.at(3)->setShinines(200);
	
	// DEBUG
	//_models.push_back(std::make_shared<Model>("Models/floor.obj"));			//6	//floor
	//_models.at(6)->_drawInstanced = true;
	//_models.at(1)->_drawInstanced = true;

	_skybox = std::make_shared<Skybox>();
	generateGameBoard();

	_crater = (_models.at(6)->createInstance(Model::DEFAULT));		// crater
	_crater->translate(glm::vec3(0.0, -2.01, 0.0));
	_crater->scale(glm::vec3(700.0, 200.0, 700.0));

	_score.push_back(_models.at(5)->createInstance(Model::DEFAULT));
	_score.back()->generateTexture = false;
	_score.push_back(_models.at(5)->createInstance(Model::DEFAULT));
	_score.back()->generateTexture = false;
	_score.push_back(_models.at(5)->createInstance(Model::DEFAULT));
	_score.back()->generateTexture = false;
}

void Renderer::clearGameObjects()
{
	_gameObjects.erase(std::remove_if(_gameObjects.begin(), _gameObjects.end(), [](const std::shared_ptr<GameObject> &object) { return object->deleteThis; }), _gameObjects.end());
}

void Renderer::framebufferSizeChangeCallback(GLFWwindow * window, int width, int height)
{
	glViewport(0, 0, width, height);
	_width = width;
	_height = height;
}

void Renderer::mouseCallbackM(GLFWwindow * window, double xpos, double ypos)
{
	if (_firstMouse)
	{
		lastX = xpos;
		lastY = ypos;
		_firstMouse = false;
		return;
	}

	float xoffset = lastX - xpos;
	float yoffset = lastY - ypos;

	lastX = xpos;
	lastY = ypos;

	_camera.rotateCamera(AROUND_X, yoffset);
	_camera.rotateCamera(AROUND_Y, xoffset);
}

static int frames = 0;
static float frameTime = 0.0;
void Renderer::frametimeLogic()
{
	float currentFrame = glfwGetTime();
	_deltaFrameTime = currentFrame - _lastFrameTime;
	_lastFrameTime = currentFrame;
	
	frames++;
	if (frameTime < 1.0) {
		frameTime += _deltaFrameTime;
	} 
	else {
		measurements.push_back(frames);
		float fps = 0;
		for (auto &f : measurements) {
			fps += f;
		}
		std::cout << fps / measurements.size() << " " << frames << std::endl;
		frames = 0;
		frameTime = 0.0;
	}
}

