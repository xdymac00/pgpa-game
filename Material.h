#pragma once

#include "Texture.h"
#include "ShaderProgram.h"

#include <assimp\scene.h>
#include <string>

class Material
{
public:
	Material(const aiMaterial *materialAi, std::string path);
	~Material();

	void useMaterial(const ShaderProgram* shaderProgram);
	void setShinines(int value);
private:
	Texture loadType(const aiMaterial *materialAi, aiTextureType type);

	Texture _diffuseMap;
	Texture _specularMap;
	Texture _ambientMap;
	Texture _normalMap;
	Texture _dispMap;
	std::string _path;
	int _shinines = 32;
};

