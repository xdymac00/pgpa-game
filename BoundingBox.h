#pragma once

#include <glad\glad.h>

#include "ShaderProgram.h"
#include "Vertex.h"
#include "DebugDefines.h"

class BoundingBox
{
public:
	BoundingBox();
	~BoundingBox();
	void resetValues();

	bool expand(const glm::vec3);
	bool expand(const Vertex vertex);
	bool expand(const BoundingBox *boundingBox);

	glm::vec3 getMinPoints() const;
	glm::vec3 getMaxPoints() const;

	bool axisAlignedColision(const BoundingBox* boundingBox);
	bool isPointAbove(glm::vec3 point) const;

#ifdef DRAW_BOUNDING_BOXES
	void draw(const ShaderProgram* shaderProgram);
	void initForDraw(/*const ShaderProgram* shaderProgram*/);
	void updateBuffers();
	void setColor(const glm::vec4 value);
#endif //DRAW_BOUNDINX_BOXES

private:
	float _minX =  FLT_MAX;
	float _maxX = -FLT_MAX;
	float _minY =  FLT_MAX;
	float _maxY = -FLT_MAX;
	float _minZ =  FLT_MAX;
	float _maxZ = -FLT_MAX;

#ifdef DRAW_BOUNDING_BOXES
	void calculateBoundingBoxGeometry();
	Vertex _vertices[8];
	static const GLuint _indices[16];
	glm::vec4 _color = { 0.0, 1.0, 0.0, 1.0 };

	bool _initializedForDraw = false;
	unsigned int _VAO;
	unsigned int _VBO;
	unsigned int _EBO;
#endif //DRAW_BOUNDINX_BOXES
};


