#include "GameObject.h"


GameObject::GameObject(std::shared_ptr<Model> model)
{
	//id = GameObject::nextID;
	//(GameObject::nextID)++;
	_model = model;
	//saveToVector(_model->getInstancesVector());

	_identityMatrix[0][0] = 1.0;
	_identityMatrix[1][1] = 1.0;
	_identityMatrix[2][2] = 1.0;
	_identityMatrix[3][3] = 1.0;

	_modelMatrix = _identityMatrix;
	_animationMatrix = _identityMatrix;

	//_instanceMatrix = _model->addInstance(getModelMatrix());

	calculateAxisAlignedBoundingBox();
#ifdef DRAW_BOUNDING_BOXES
	_boundingBox.initForDraw();
#endif // DRAW_BOUNDING_BOXES
#ifdef BOUNDING_VOLUMES
	_boundingVolume.expand(_model->getBoundingVolume());
	_boundingVolume.calculateVertices();
#endif // BOUNDING_VOLUMES
}

std::shared_ptr<GameObject> GameObject::createObject(std::shared_ptr<Model> model)
{
	std::shared_ptr<GameObject> ret = std::make_shared<GameObject>(model);
	//ret->init(ret);
	return ret;
}

/*void GameObject::init(std::shared_ptr<GameObject> thi)
{
	saveToVector(_model->getInstancesVector(), thi);
}*/

GameObject::~GameObject()
{
	//_model->removeInstance(_instanceMatrix);
	//for (auto &vector : _savedIn) {
	//for (auto &vector : _savedIn) {
	//	int i = distance(vector->begin(), find(vector->begin(), vector->end(), /*this*/ enable_shared_from_this::shared_from_this()));
	//	vector->erase(vector->begin() + i);
	//}

}

void GameObject::draw(const ShaderProgram* shaderProgram) const
{
	shaderProgram->setUniformVariable("modelMatrix", _modelMatrix * _animationMatrix);
	//shaderProgram->setUniformVariable("gs_state", getGSstate());
	//shaderProgram->setUniformVariable("generateTexture", generateTexture);
	_model->draw(shaderProgram);
}

glm::mat4 GameObject::getObjectMatrix()
{
	return _modelMatrix * _animationMatrix;
}

#ifdef DRAW_BOUNDING_BOXES
void GameObject::drawBoundingBox(const ShaderProgram * shaderProgram)
{
	shaderProgram->setUniformVariable("modelMatrix", _identityMatrix);
	calculateAxisAlignedBoundingBox();
	_boundingBox.updateBuffers();
	_boundingBox.draw(shaderProgram);
	shaderProgram->setUniformVariable("modelMatrix", _modelMatrix * _animationMatrix);
	_model->drawBoundingBox(shaderProgram);
}
#endif //DRAW_BOUNDING_BOXES

bool GameObject::colide(GameObject* gameObject)
{
	if (!coliding() || !gameObject->coliding()) { return false; }
	calculateAxisAlignedBoundingBox();
	gameObject->calculateAxisAlignedBoundingBox();
	bool ret = _boundingBox.axisAlignedColision(&(gameObject->_boundingBox));
	if (ret) { 
		gameObject->onColision(this);	// first will resolve colision on external object
		onColision(gameObject);			// than on this object
	}
	return ret && gameObject->coliding();
}

#ifdef BOUNDING_VOLUMES
bool GameObject::colideWith(GameObject * gameObject)
{
	return _boundingVolume.colideWith(&gameObject->_boundingVolume);
}
#endif //BOUNDING_VOLUMES

void GameObject::translate(const glm::vec3 value)
{
	_modelMatrix = glm::translate(_modelMatrix, value);
//	_objectChange = true;
#ifdef BOUNDING_VOLUMES
	transformBoundingVolume();
#endif //BOUNDING_VOLUMES
}

void GameObject::scale(const glm::vec3 value)
{
	_modelMatrix = glm::scale(_modelMatrix, value);
//	_objectChange = true;
#ifdef BOUNDING_VOLUMES
	transformBoundingVolume();
#endif //BOUNDING_VOLUMES
}

void GameObject::rotate(const glm::vec3 value, const float angle)
{
	_modelMatrix = glm::rotate(_modelMatrix, glm::radians(angle), value);
//	_objectChange = true;
#ifdef BOUNDING_VOLUMES
	transformBoundingVolume();
#endif //BOUNDING_VOLUMES
}

#ifdef BOUNDING_VOLUMES
bool GameObject::colideWith(GameObject * gameObject)
{
	return _boundingVolume.colideWith(&gameObject->_boundingVolume);
}
#endif //BOUNDING_VOLUMES

void GameObject::translateAnim(const glm::vec3 value)
{
	_animationMatrix = glm::translate(_animationMatrix, value);
//	_objectChange = true;
#ifdef BOUNDING_VOLUMES
	transformBoundingVolume();
#endif //BOUNDING_VOLUMES
}

void GameObject::scaleAnim(const glm::vec3 value)
{
	_animationMatrix = glm::scale(_animationMatrix, value);
//	_objectChange = true;
#ifdef BOUNDING_VOLUMES
	transformBoundingVolume();
#endif //BOUNDING_VOLUMES
}

void GameObject::rotateAnim(const glm::vec3 value, const float angle)
{
	_animationMatrix = glm::rotate(_animationMatrix, glm::radians(angle), value);
//	_objectChange = true;
#ifdef BOUNDING_VOLUMES
	transformBoundingVolume();
#endif //BOUNDING_VOLUMES
}

void GameObject::resetAnim()
{
	_animationMatrix = getIdentityMatrix();
#ifdef BOUNDING_VOLUMES
	transformBoundingVolume();
#endif //BOUNDING_VOLUMES
}

void GameObject::setAnimation(std::function<void(float)> value)
{
	_animation = value;
}

glm::mat4 GameObject::getModelMatrix() const
{
	return _modelMatrix;
}

void GameObject::onColision(GameObject* object)
{
}

bool GameObject::coliding()
{
	return true;
}

bool GameObject::isPointAbove(glm::vec3 point) const
{
	return _boundingBox.isPointAbove(point);
}

void GameObject::animate(float time)
{
	_animation(time);
}

glm::mat4 GameObject::getIdentityMatrix() const
{
	return _identityMatrix;
}

void GameObject::setModelMatrix(glm::mat4 value)
{
	_modelMatrix = value;
}

float GameObject::getGSstate() const
{
	return 0.0f;
}

void GameObject::calculateAxisAlignedBoundingBox()
{
	_boundingBox.resetValues();
#ifdef DRAW_BOUNDING_BOXES
	_boundingBox.setColor(glm::vec4(0.0, 0.0, 1.0, 1.0));
#endif // DRAW_BOUNDING_BOXES

	glm::vec3 minPoints = _model->getBoundingBox()->getMinPoints();
	glm::vec3 maxPoints = _model->getBoundingBox()->getMaxPoints();
	glm::mat4 tmpMat = _modelMatrix /** _animationMatrix*/;

	glm::vec4 pointH = tmpMat * glm::vec4(minPoints.x, minPoints.y, minPoints.z, 1.0);
	_boundingBox.expand(glm::vec3(pointH.x / pointH.w, pointH.y / pointH.w, pointH.z / pointH.w));
	pointH = tmpMat * glm::vec4(maxPoints.x, minPoints.y, minPoints.z, 1.0);
	_boundingBox.expand(glm::vec3(pointH.x / pointH.w, pointH.y / pointH.w, pointH.z / pointH.w));
	pointH = tmpMat * glm::vec4(minPoints.x, maxPoints.y, minPoints.z, 1.0);
	_boundingBox.expand(glm::vec3(pointH.x / pointH.w, pointH.y / pointH.w, pointH.z / pointH.w));
	pointH = tmpMat * glm::vec4(maxPoints.x, maxPoints.y, minPoints.z, 1.0);
	_boundingBox.expand(glm::vec3(pointH.x / pointH.w, pointH.y / pointH.w, pointH.z / pointH.w));
	pointH = tmpMat * glm::vec4(minPoints.x, minPoints.y, maxPoints.z, 1.0);
	_boundingBox.expand(glm::vec3(pointH.x / pointH.w, pointH.y / pointH.w, pointH.z / pointH.w));
	pointH = tmpMat * glm::vec4(maxPoints.x, minPoints.y, maxPoints.z, 1.0);
	_boundingBox.expand(glm::vec3(pointH.x / pointH.w, pointH.y / pointH.w, pointH.z / pointH.w));
	pointH = tmpMat * glm::vec4(minPoints.x, maxPoints.y, maxPoints.z, 1.0);
	_boundingBox.expand(glm::vec3(pointH.x / pointH.w, pointH.y / pointH.w, pointH.z / pointH.w));
	pointH = tmpMat * glm::vec4(maxPoints.x, maxPoints.y, maxPoints.z, 1.0);
	_boundingBox.expand(glm::vec3(pointH.x / pointH.w, pointH.y / pointH.w, pointH.z / pointH.w));
}

#ifdef BOUNDING_VOLUMES
void GameObject::transformBoundingVolume()
{
	_boundingVolume.transform(_modelMatrix);
}
#endif //BOUNDING_VOLUMES

void GameObject::setPeakHeight(float height)
{
	peakHeight = height;
}

float GameObject::getPeakheight()
{
	return peakHeight;
}

//void GameObject::saveToVector(std::vector<std::shared_ptr<GameObject>>* vector, std::shared_ptr<GameObject> thi)
//{
//	vector->push_back(/*shared_from_this()*/ thi);
//	_savedIn.push_back(vector);
//}


//////// GameObjectPlayer ////////
GameObjectPlayer::GameObjectPlayer(std::shared_ptr<Model> model) : GameObject(model)
{
	generateTexture = false;
}

std::shared_ptr<GameObject> GameObjectPlayer::createObject(std::shared_ptr<Model> model)
{
	std::shared_ptr<GameObject> ret = std::make_shared<GameObjectPlayer>(model);
	//ret->init(ret);
	return ret;
}

glm::vec3 GameObjectPlayer::getPosition() const
{
	glm::vec4 tvec = getModelMatrix() * glm::vec4(_position, 1.0);
	return glm::vec3(tvec.x / tvec.w, tvec.y / tvec.w, tvec.z / tvec.w);
}

glm::vec3 GameObjectPlayer::getFront() const
{
	glm::vec4 tvec = getModelMatrix() * glm::vec4(_front, 1.0);
	return glm::vec3(tvec.x / tvec.w, tvec.y / tvec.w, tvec.z / tvec.w) - getPosition();
}

int GameObjectPlayer::getScore()
{
	return _points;
}

void GameObjectPlayer::pickPoint()
{
	_points++;
	std::cout << _points << std::endl;

	_pointParticles->_clusterSize = 1;
	_pointParticles->updateParticles(1.0);
	_pointParticles->_clusterSize = 0;
}

static float lastTime = -1.0;
void GameObjectPlayer::animate(float time)
{
	GameObject::animate(time);
	resetAnim();
	float p = glm::sin(time) / 4;
	scaleAnim(glm::vec3(1.5-p, 2+p, 1.5-p));

	//jump animation
	static float jumpCounter = 0;
	float dTime = time - lastTime;
	if (lastTime <= 0.0) dTime = 0.0;
	lastTime = time;
	
	if (_jump) { _jump = false; jumpCounter = 1.0; }
	if (jumpCounter > 0) {
		scaleAnim(glm::vec3(1.0 - jumpCounter/2, 1.0 + jumpCounter, 1.0 - jumpCounter/2));
		jumpCounter -= dTime; // 0.02;
	}

	static float moveCounter = 0;
	if (_move && moveCounter <= 0.0001) { _move = false; moveCounter = 1.0; }
	if (moveCounter > 0) {
		float f = (moveCounter < 0.5) ? moveCounter : (1.0 - moveCounter);
		scaleAnim(glm::vec3(1.0 - f/4, 1.0 - f/4, 1.0 + f ));
		moveCounter -= dTime/2;
		_move = false;
	}
}

void GameObjectPlayer::draw(const ShaderProgram * shaderProgram) const
{
	GameObject::draw(shaderProgram);
}

//////// GameObjectCoin ////////

GameObjectCoin::GameObjectCoin(std::shared_ptr<Model> model) : GameObject(model)
{
	generateTexture = false;
}

std::shared_ptr<GameObject> GameObjectCoin::createObject(std::shared_ptr<Model> model)
{
	std::shared_ptr<GameObject> ret = std::make_shared<GameObjectCoin>(model);
	//ret->init(ret);
	return ret;
}

GameObjectCoin::~GameObjectCoin()
{
	//GameObject::~GameObject();
}

void GameObjectCoin::onPickUp()
{
	_pickedUp = true;
}

void GameObjectCoin::onColision(GameObject * object)
{
	GameObjectPlayer* player = dynamic_cast<GameObjectPlayer*>(object);
	if (player != nullptr) {
		onPickUp();
		player->pickPoint();
	}
}

void GameObjectCoin::animate(float time)
{

	GameObject::animate(time);
	resetAnim();
	rotateAnim(glm::vec3(0.0, 1.0, 0.0), 100* time);
	translateAnim(glm::vec3(0.0, glm::sin(time) / 4, 0.0));

	//end Animation
	if (_pickedUp) {
		_gsState +=  time - lastTime;
		_light->setDiffuseStr((_light->_diffuseStr + glm::vec3(0.5, 0.5, 0.5)) * (1-_gsState));
		scaleAnim(glm::vec3(1-_gsState, 1 - _gsState, 1 - _gsState));
		if (_gsState > 1.0) {
			deleteThis = true;
			//delete this;
		}
	}
	lastTime = time;
}

bool GameObjectCoin::coliding()
{
	return !_pickedUp;
}

void GameObjectCoin::draw( const ShaderProgram * shaderProgram) const
{	
	GameObject::draw(shaderProgram);
}

float GameObjectCoin::getGSstate() const
{
	return _gsState;
}
