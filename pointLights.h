#pragma once

#include "Light.h"
#include "ShaderProgram.h"
#include <memory>
#include <algorithm>

struct LightParam {
	glm::mat4 lightVolumeMatrix;
	glm::vec3 position;
	glm::vec3 color;
	float linear;
	float qudratic;
};

class PointLights
{
public:
	PointLights();
	~PointLights();

	void addLight(std::shared_ptr<PointLight> light);
	void clearLights();	// remove pointers on destroyed objects

	void drawLightVolumes(ShaderProgram* program);

private:
	std::vector<std::weak_ptr<PointLight>> _lights;

	GLuint _cubeVBO, _instanceBuffer, _cubeVAO = 0;
	static const std::vector<float> _cubeVertices;
};

