#pragma once

#include "Mesh.h"
#include "BoundingVolume.h"
#include "GameObject.h"

#include <glm/glm.hpp>

#include <assimp\Importer.hpp>
#include <assimp\scene.h>
#include <assimp\postprocess.h>

#include <string>
#include <vector>
#include <memory>

class GameObject;

class Model : public std::enable_shared_from_this<Model>
{
public:
	enum types
	{
		DEFAULT, PLAYE, COIN
	};

	Model(const char*);
	~Model();

	void draw(const ShaderProgram* shaderProgram) const;

	void drawInstanced(const ShaderProgram* shaderProgram	);
	//std::vector<std::weak_ptr<GameObject>>* getInstancesVector();
	std::shared_ptr<GameObject> Model::createInstance(types type);
	//void updateInstance(glm::mat4* instance, glm::mat4 objectMatrix);
	//void removeInstance(glm::mat4* instance);

	const BoundingBox* getBoundingBox() const;
	void setShinines(int value);
	void setTexCoordMultiplier(float value);
	bool _drawInstanced = false;
	bool _inverseNormals = false;

#ifdef BOUNDING_VOLUMES
	const BoundingVolume* getBoundingVolume() const;
#endif //BOUNDING_VOLUMES
#ifdef DRAW_BOUNDING_BOXES
	void drawBoundingBox(const ShaderProgram* shaderProgram);
#endif //DRAW_BOUNDING_BOXES
private:
	void processNode(aiNode *node, const aiScene *scene, std::string path);
	Mesh* processMesh(aiMesh *mesh, const aiScene *scene,std::string path);
	glm::vec3 aiVecToGlmVec(aiVector3D vectorIn);
	glm::vec2 aiVecToGlmVec(aiVector2D vectorIn);
	
	std::vector<std::weak_ptr<GameObject>> _instances;
	void clearInstances();	//remove from _instances pointers to non existing object
	
	unsigned int _instanceBuffer = 0;

	std::vector<Mesh*> _meshes;
	BoundingBox _boundingBox;
#ifdef BOUNDING_VOLUMES
	BoundingVolume _boundingVolume;
#endif //BOUNDING_VOLUMES
	
};