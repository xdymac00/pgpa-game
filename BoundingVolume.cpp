#include "BoundingVolume.h"



BoundingVolume::BoundingVolume()
{
	//init BoundingVolume
	_halfSpaces.push_back(HalfSpace(glm::vec3(FLT_MAX, FLT_MAX, FLT_MAX), glm::vec3(-1.0, 0.0, 0.0)));
	_halfSpaces.push_back(HalfSpace(glm::vec3(FLT_MAX, FLT_MAX, FLT_MAX), glm::vec3(0.0, -1.0, 0.0)));
	_halfSpaces.push_back(HalfSpace(glm::vec3(FLT_MAX, FLT_MAX, FLT_MAX), glm::vec3(0.0, 0.0, -1.0)));

	_halfSpaces.push_back(HalfSpace(glm::vec3(-FLT_MAX, -FLT_MAX, -FLT_MAX), glm::vec3(1.0, 0.0, 0.0)));
	_halfSpaces.push_back(HalfSpace(glm::vec3(-FLT_MAX, -FLT_MAX, -FLT_MAX), glm::vec3(0.0, 1.0, 0.0)));
	_halfSpaces.push_back(HalfSpace(glm::vec3(-FLT_MAX, -FLT_MAX, -FLT_MAX), glm::vec3(0.0, 0.0, 1.0)));
}

BoundingVolume::~BoundingVolume()
{
}

void BoundingVolume::expand(const glm::vec3 point)
{
	for (auto &halfSpace : _halfSpaces) {
		if (!halfSpace.colideWith(point)) {
			halfSpace.setPoint(point);
		}
	}
}

void BoundingVolume::expand(const BoundingVolume* boundingVolume)
{
	for (const auto &vertex : boundingVolume->_verticesTransformed) {
		expand(vertex);
	}
}

void BoundingVolume::calculateVertices()
{
	std::vector<glm::vec3> intersections;
	for (int i = 0; i < _halfSpaces.size(); i++) {
		for (int j = i + 1; j < _halfSpaces.size(); j++) {
			for (int k = j + 1; k < _halfSpaces.size(); k++) {
				glm::vec3 pom = HalfSpace::calculate3HalfSpaceIntersection(&_halfSpaces.at(i), &_halfSpaces.at(j), &_halfSpaces.at(k));
				if (!std::isinf(pom.x)) { intersections.push_back(pom); } 
			}
		}
	}

	for (auto &intersection : intersections) {
		if (isVertexIn(intersection)) {
			_verticesOriginal.push_back(intersection);
		}
	}

	_verticesTransformed = _verticesOriginal;
}

bool BoundingVolume::colideWith(BoundingVolume * boundryVolume) const
{
	for (const auto &vertex : boundryVolume->_verticesTransformed) {
		if (isVertexIn(vertex)) { return true; }
	}
	for (const auto &vertex : _verticesTransformed) {
		if (boundryVolume->isVertexIn(vertex)) { return true; }
	}

	return false;
}

void BoundingVolume::transform(glm::mat4 transformations, bool aplyOnTransformed)
{
	//for (auto &vertex : (aplyOnTransformed) ? _verticesTransformed : _verticesOriginal) {
	for (int i = 0; i < _verticesOriginal.size(); i++) {
		glm::vec4 tmp = transformations * glm::vec4(((aplyOnTransformed) ? _verticesTransformed[i] : _verticesOriginal[i]), 1.0);
		_verticesTransformed[i] = glm::vec3(tmp.x / tmp.w, tmp.y / tmp.w, tmp.z / tmp.w);
	}
}

#ifdef DRAW_BOUNDING_BOXES
void BoundingVolume::draw(ShaderProgram * shaderProgram) const
{
	//todo: BoundingVolume draw
}
#endif // DRAW_BOUNDING_BOXES

bool BoundingVolume::isVertexIn(glm::vec3 vertex) const
{
	bool isIn = true;
	for (const auto &space : _halfSpaces) {
		isIn &= space.colideWith(vertex);
	}
	return isIn;
}


//////////// HalfSpace //////////// 

HalfSpace::HalfSpace(glm::vec3 point, glm::vec3 normal)
{
	_point = point;
	_normal = normal;
}

HalfSpace::~HalfSpace()
{
}

bool HalfSpace::colideWith(glm::vec3 point) const
{
	return (glm::dot(glm::vec3(point - _point), _normal) <= 0) ? true : false;
}

void HalfSpace::setPoint(const glm::vec3 value)
{
	_point = value;
}

glm::vec3 HalfSpace::calculate3HalfSpaceIntersection(const HalfSpace * hs1, const HalfSpace * hs2, const HalfSpace * hs3)
{
	float a1 = hs1->_normal.x;
	float b1 = hs1->_normal.y;
	float c1 = hs1->_normal.z;
	float d1 = -glm::dot(hs1->_normal, hs1->_point);

	float a2 = hs2->_normal.x;
	float b2 = hs2->_normal.y;
	float c2 = hs2->_normal.z;
	float d2 = -glm::dot(hs2->_normal, hs2->_point);

	float a3 = hs3->_normal.x;
	float b3 = hs3->_normal.y;
	float c3 = hs3->_normal.z;
	float d3 = -glm::dot(hs3->_normal, hs3->_point);


	//float e = (-a2 * b1 / a1) + b2;
	/*float z = ((((a3 * b1 * a2 * d1 - b3 * a2 * d1) / a1 - a3 * b1 * d2 + b3 * d2) / e) + (a3 * d1 / a1) + d3) /
		((((a3 * b1 * a2 * c1 + b3 * a2 * c1) / a1 + a3 * b1 * c2 - b3 * c2) / e) - (a3 * c1 / a1) + c3);
	float y =	(((a2 * c1 * z) / a1) + ((a2 * d1) / a1) - (c2 * z) - d2 ) / e;
	float x = (-b1 * y - c1 * z - d1) / (a1);*/

	//http://cplusplus.happycodings.com/mathematics/code5.html
	float D = (a1*b2*c3 + b1*a3*c2 + c1*a2*b3) - (a1*c2*b3 + b1*a2*c3 + c1*b2*a3);
	if (D == 0) { float inf = INFINITY; return glm::vec3(inf, inf, inf); }
	float x = ((b1*c3*d2 + c1*b2*d3 + d1*c2*b3) - (b1*c2*d3 + c1*b3*d2 + d1*b2*c3)) / D;
	float y = ((a1*c2*d3 + c1*a3*d2 + d1*a2*c3) - (a1*c3*d2 + c1*a2*d3 + d1*c2*a3)) / D;
	float z = ((a1*b3*d2 + b1*a2*d3 + d1*b2*a3) - (a1*b2*d3 + b1*a3*d2 + d1*a2*b3)) / D;

	return glm::vec3(x, y, z);
}
