#include "Material.h"


Material::Material(const aiMaterial *materialAi, std::string path): _path(path)
{
	_diffuseMap = loadType(materialAi, aiTextureType_DIFFUSE);
	_specularMap = loadType(materialAi, aiTextureType_SPECULAR);	
	_ambientMap = loadType(materialAi, aiTextureType_AMBIENT);
	_normalMap = loadType(materialAi, aiTextureType_HEIGHT);		//normal Map
	_dispMap = loadType(materialAi, aiTextureType_DISPLACEMENT);
}

Material::~Material()
{
}

void Material::useMaterial(const ShaderProgram* shaderProgram)
{
	if (_diffuseMap.use) {
		glActiveTexture(GL_TEXTURE0);
		glUniform1i(glGetUniformLocation(shaderProgram->getShaderProgram(), "material.diffuse"), 0);
		glBindTexture(GL_TEXTURE_2D, _diffuseMap.id);
	}
	if (_specularMap.use) {
		glActiveTexture(GL_TEXTURE1);
		glUniform1i(glGetUniformLocation(shaderProgram->getShaderProgram(), "material.specular"), 1);
		glBindTexture(GL_TEXTURE_2D, _specularMap.id);
		shaderProgram->setUniformVariable("material.useSpecular", true);
	}
	else {
		shaderProgram->setUniformVariable("material.useSpecular", false);
	}
	if (_ambientMap.use) {
		glActiveTexture(GL_TEXTURE2);
		glUniform1i(glGetUniformLocation(shaderProgram->getShaderProgram(), "material.ambient"), 2);
		glBindTexture(GL_TEXTURE_2D, _ambientMap.id);
		shaderProgram->setUniformVariable("material.useAmbient", true);
	}
	else {
		shaderProgram->setUniformVariable("material.useAmbient", false);
	}
	if (_normalMap.use) {
		glActiveTexture(GL_TEXTURE3);
		glUniform1i(glGetUniformLocation(shaderProgram->getShaderProgram(), "material.normal"), 3);
		glBindTexture(GL_TEXTURE_2D, _normalMap.id);
		shaderProgram->setUniformVariable("material.useNormal", true);
	}
	else {
		shaderProgram->setUniformVariable("material.useNormal", false);
	}
	if (_dispMap.use) {
		glActiveTexture(GL_TEXTURE4);
		glUniform1i(glGetUniformLocation(shaderProgram->getShaderProgram(), "material.displacement"), 4);
		glBindTexture(GL_TEXTURE_2D, _dispMap.id);
		shaderProgram->setUniformVariable("material.useDisplacemnet", true);
	}
	else {
		shaderProgram->setUniformVariable("material.useDisplacemnet", false);
	}
	shaderProgram->setUniformVariable("material.shininess", (float) _shinines);
}

void Material::setShinines(int value)
{
	_shinines = value;
}

Texture Material::loadType(const aiMaterial *materialAi, aiTextureType type)
{
	if (materialAi->GetTextureCount(type) > 0) {
		aiString str;
		materialAi->GetTexture(type, 0, &str);
		//std::cout << type << " - " << str.C_Str() << std::endl;
		Texture texture(_path + "/" + str.C_Str());
		return texture;
	}
	return Texture();
}
