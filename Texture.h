#pragma once

#include <glad\glad.h>
#include <GLFW\glfw3.h>
#include <assimp\Importer.hpp>
#include <iostream>


class Texture 
{
public:
	Texture();
	Texture(const std::string path);
	~Texture();

	unsigned int id;
	bool use = false;
	//std::string type;
	//aiString path;
};