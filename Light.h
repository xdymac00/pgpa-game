#pragma once

#include "ShaderProgram.h"
#include <iostream>

class Light
{
public:
	Light();
	~Light();

	virtual void useLight(const ShaderProgram* shaderProgram, const int lightID) const;

	//getters
	glm::vec3 getAmbientStr() const;
	glm::vec3 getDiffuseStr() const;
	glm::vec3 getSpecularStr() const;
	/*glm::vec3 getPosition() const;*/
	//setters
	void setAmbientStr(const glm::vec3 str);
	void setDiffuseStr(const glm::vec3 str);
	void setSpecularStr(const glm::vec3 str);

	virtual std::string getClassPrefix() const;
	glm::vec3 _ambientStr = glm::vec3(0.8, 0.8, 0.8);
	glm::vec3 _diffuseStr = glm::vec3(0.8, 0.8, 0.8);
	glm::vec3 _specularStr = glm::vec3(0.8, 0.8, 0.8);

private:

	std::string _classPrefix = "";

	//glm::vec3 _position = glm::vec3(1.0, 1.0, 1.0);
};

#include <string>

class DirectionLight : public Light
{
public:
	DirectionLight();
	~DirectionLight();

	void useLight(const ShaderProgram* shaderProgram, const int lightID) const;

	void setDirection(const glm::vec3 value);
	void changeDirection(const glm::vec3 value);

	std::string getClassPrefix() const;
private:
	glm::vec3 _direction = glm::vec3(0.0, -1.0, 0.0);
	std::string _classPrefix = "directionLights";
};

class PointLight: public Light 
{
public:
	PointLight();
	~PointLight();

	virtual void useLight(const ShaderProgram* shaderProgram, const int lightID) const;

	void setConstant(const float value);
	void setLinear(const float value);
	void setQuadratic(const float value);
	void setPosition(const glm::vec3 value);
	float getRadius();

	virtual std::string getClassPrefix() const;
	glm::mat4 _lightVolumeModelMatrix;
	glm::vec3 _position = glm::vec3(0.0, 0.0, 0.0);
	float _linear =		0.14;
	float _quadratic =	0.2;
private:
	float _constant = 1.0;
	
	

	std::string _classPrefix = "pointLights";

	GLuint _cubeVBO, _cubeVAO = 0;
	static const std::vector<float> _cubeVertices;
};