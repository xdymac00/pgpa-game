#include "pointLights.h"



PointLights::PointLights()
{
	glGenVertexArrays(1, &_cubeVAO);
	glGenBuffers(1, &_cubeVBO);
	glGenBuffers(1, &_instanceBuffer);
	glBindVertexArray(_cubeVAO);
	glBindBuffer(GL_ARRAY_BUFFER, _cubeVBO);
	glBufferData(GL_ARRAY_BUFFER, _cubeVertices.size() * sizeof(float), _cubeVertices.data(), GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));

	glBindBuffer(GL_ARRAY_BUFFER, _instanceBuffer);
	glEnableVertexAttribArray(3);
	glEnableVertexAttribArray(4);
	glEnableVertexAttribArray(5);
	glEnableVertexAttribArray(6);
	glEnableVertexAttribArray(7);
	glEnableVertexAttribArray(8);
	glEnableVertexAttribArray(9);
	glEnableVertexAttribArray(10);
	//lightVolumeMatrix
	glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE,  sizeof(LightParam), (void*)0);
	glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE,  sizeof(LightParam), (void*)(1 * sizeof(glm::vec4)));
	glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE,  sizeof(LightParam), (void*)(2 * sizeof(glm::vec4)));
	glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE,  sizeof(LightParam), (void*)(3 * sizeof(glm::vec4)));
	//rest of param									
	glVertexAttribPointer(7,  3, GL_FLOAT, GL_FALSE, sizeof(LightParam), (void*)(4 * sizeof(glm::vec4)));	//position
	glVertexAttribPointer(8,  3, GL_FLOAT, GL_FALSE, sizeof(LightParam), (void*)(4 * sizeof(glm::vec4) + 1 * sizeof(glm::vec3)));	//color
	glVertexAttribPointer(9,  1, GL_FLOAT, GL_FALSE, sizeof(LightParam), (void*)(4 * sizeof(glm::vec4) + 2 * sizeof(glm::vec3)));	//linear
	glVertexAttribPointer(10, 1, GL_FLOAT, GL_FALSE, sizeof(LightParam), (void*)(4 * sizeof(glm::vec4) + 2 * sizeof(glm::vec3) + sizeof(float)));	//quadratic

	glVertexAttribDivisor(3, 1);
	glVertexAttribDivisor(4, 1);
	glVertexAttribDivisor(5, 1);
	glVertexAttribDivisor(6, 1);
	glVertexAttribDivisor(7, 1);
	glVertexAttribDivisor(8, 1);
	glVertexAttribDivisor(9, 1);
	glVertexAttribDivisor(10, 1);
}


PointLights::~PointLights()
{
	glDeleteBuffers(1, &_cubeVAO);
	glDeleteBuffers(1, &_cubeVBO);
}

void PointLights::addLight(std::shared_ptr<PointLight> light)
{
	_lights.push_back(light);
}

void PointLights::clearLights()
{
	_lights.erase(std::remove_if(_lights.begin(), _lights.end(), [](std::weak_ptr<PointLight> const &object) { return (object.expired() || !object.lock()); }), _lights.end());
}

void PointLights::drawLightVolumes(ShaderProgram * program)
{
	std::vector<LightParam> lightsParam;
	clearLights();	//all instances are valid
	//for (auto &light : _lights) {
	//	std::shared_ptr<PointLight> shLight = light.lock();
	//	lightsParam.push_back({ glm::scale(shLight->_lightVolumeModelMatrix, glm::vec3(5.0, 5.0, 5.0))/*shLight->_lightVolumeModelMatrix*/, shLight->_position, shLight->_diffuseStr, shLight->_linear, shLight->_quadratic });
	//	//_lightVolumeModelMatrix = glm::scale(_lightVolumeModelMatrix, glm::vec3(2.0, 2.0, 2.0));
	//}
	std::for_each(_lights.begin(), _lights.end(), [&lightsParam](std::weak_ptr<PointLight> light) {
		std::shared_ptr<PointLight> shLight = light.lock();		
		float radius = shLight->getRadius();
		lightsParam.push_back({glm::scale(shLight->_lightVolumeModelMatrix, glm::vec3(radius, radius, radius)), shLight->_position, shLight->_diffuseStr, shLight->_linear, shLight->_quadratic });
	});

	glBindBuffer(GL_ARRAY_BUFFER, _instanceBuffer);
	glBufferData(GL_ARRAY_BUFFER, lightsParam.size() * sizeof(LightParam), lightsParam.data(), GL_DYNAMIC_DRAW);
	

	glBindVertexArray(_cubeVAO);
	glDrawArraysInstanced(GL_TRIANGLES, 0, 36, lightsParam.size());
	//glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);

}

const std::vector<float> PointLights::_cubeVertices = {
		// back face
		-1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f, // bottom-left
		1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f, // top-right
		1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 0.0f, // bottom-right         
		1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f, // top-right
		-1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f, // bottom-left
		-1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 1.0f, // top-left
		// front face
		-1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, // bottom-left
		1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 0.0f, // bottom-right
		1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f, // top-right
		1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f, // top-right
		-1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 1.0f, // top-left
		-1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, // bottom-left
		// left face
		-1.0f,  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-right
		-1.0f,  1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 1.0f, // top-left
		-1.0f, -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-left
		-1.0f, -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-left
		-1.0f, -1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 0.0f, // bottom-right
		-1.0f,  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-right
		// right face
		1.0f,  1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-left
		1.0f, -1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-right
		1.0f,  1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 1.0f, // top-right         
		1.0f, -1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-right
		1.0f,  1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-left
		1.0f, -1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 0.0f, // bottom-left     
		// bottom face
		-1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f, // top-right
		1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 1.0f, // top-left
		1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f, // bottom-left
		1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f, // bottom-left
		-1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 0.0f, // bottom-right
		-1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f, // top-right
		// top face
		-1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, // top-left
		1.0f,  1.0f , 1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f, // bottom-right
		1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 1.0f, // top-right     
		1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f, // bottom-right
		-1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, // top-left
		-1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 0.0f  // bottom-left        
	};