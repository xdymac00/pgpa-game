#include "ShaderProgram.h"



ShaderProgram::ShaderProgram()
{
}

ShaderProgram::~ShaderProgram()
{
	glDeleteProgram(_shaderProgram);
}

bool ShaderProgram::addShader(char * shaderPath, GLenum shaderType)
{
	Shader* shader = new Shader(shaderPath, shaderType);
	_shaders.push_back(shader);
	return true;
}

bool ShaderProgram::linkProgram()
{
	_shaderProgram = glCreateProgram();											//vytvoreni shader programu
	for (auto shader : _shaders) {												//pridani shaderu k programu
		glAttachShader(_shaderProgram, shader->getShader());
	}
	glLinkProgram(_shaderProgram);												//slinkovani programu

	//osetreni chybovych stavu
	int linkingStatus;
	glGetProgramiv(_shaderProgram, GL_LINK_STATUS, &linkingStatus);				//load linking status
	if (linkingStatus == GL_FALSE) {
		GLint maxLength = 0;
		glGetProgramiv(_shaderProgram, GL_INFO_LOG_LENGTH, &maxLength);			//load errorLog length

		std::vector<GLchar> errorLog(maxLength);
		glGetProgramInfoLog(_shaderProgram, maxLength, &maxLength, &errorLog[0]);	//load errorLog
		std::cout << "PROGRAM LINKING ERROR" << std::endl;
		for (auto i : errorLog)													//print errorLog
			std::cout << i;

		glDeleteProgram(_shaderProgram);

		throw std::runtime_error("shader compilation failed");
	}
	deleteShaders();
	return true;
}

void ShaderProgram::deleteShaders()
{
	_shaders.clear();
}

void ShaderProgram::useProgram()
{
	glUseProgram(_shaderProgram);
}

////////////// seting uniform variables ////////////// 
void ShaderProgram::setUniformVariable(const std::string &name, bool value) const
{
	glUniform1i(glGetUniformLocation(_shaderProgram, name.c_str()), (int)value);
}

void ShaderProgram::setUniformVariable(const std::string &name, GLint value) const
{
	glUniform1i(glGetUniformLocation(_shaderProgram, name.c_str()), value);
}

void ShaderProgram::setUniformVariable(const std::string &name, GLfloat value) const
{
	glUniform1f(glGetUniformLocation(_shaderProgram, name.c_str()), value);
}

void ShaderProgram::setUniformVariable(const std::string & name, glm::vec4 value) const
{
	glUniform4fv(glGetUniformLocation(_shaderProgram, name.c_str()), 1, glm::value_ptr(value));
}

void ShaderProgram::setUniformVariable(const std::string & name, glm::vec2 value) const
{
	glUniform2fv(glGetUniformLocation(_shaderProgram, name.c_str()), 1, glm::value_ptr(value));
}

void ShaderProgram::setUniformVariable(const std::string & name, glm::vec3 value) const
{
	glUniform3fv(glGetUniformLocation(_shaderProgram, name.c_str()), 1, glm::value_ptr(value));
}

void ShaderProgram::setUniformVariable(const std::string & name, glm::mat4 value) const
{
	glUniformMatrix4fv(glGetUniformLocation(_shaderProgram, name.c_str()), 1, GL_FALSE, glm::value_ptr(value));
}

void ShaderProgram::setUniformVariable(const std::string & name, std::vector<glm::mat4>& value) const
{
	glUniformMatrix4fv(glGetUniformLocation(_shaderProgram, name.c_str()), value.size(), GL_FALSE, glm::value_ptr((value.data()[0])));
}

void ShaderProgram::setUniformVariable(const std::string & name, std::vector<glm::vec3>& value) const
{
	glUniform3fv(glGetUniformLocation(_shaderProgram, name.c_str()), value.size(), glm::value_ptr((value.data()[0])));
}

////////////// ////////////// //////////////

GLuint ShaderProgram::getShaderProgram() const
{
	return _shaderProgram;
}
